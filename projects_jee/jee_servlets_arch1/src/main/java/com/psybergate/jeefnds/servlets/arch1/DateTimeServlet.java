package com.psybergate.jeefnds.servlets.arch1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

@WebServlet(name = "datetime", urlPatterns = "/datetime")
public class DateTimeServlet extends HttpServlet {
  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.setContentType("text/html");

    PrintWriter writer = resp.getWriter();
    writer.println("<html><body>");
    writer.println("<p>" + LocalDate.now() + "<p>");
    writer.println("</html></body>");
  }
}
