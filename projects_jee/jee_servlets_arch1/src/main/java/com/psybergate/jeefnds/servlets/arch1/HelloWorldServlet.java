package com.psybergate.jeefnds.servlets.arch1;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "HelloWorld", urlPatterns = "/helloworld")
public class HelloWorldServlet extends HttpServlet {
  public HelloWorldServlet() {
    System.out.println("Servlet constructor...");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.setContentType("text/html");

    PrintWriter writer = resp.getWriter();
    writer.println("<html><body>");
    writer.println("<h1>Hello World</h1>");
    writer.println("</body><html>");

  }
}
