package com.psybergate.jeefnds.servlets.hwweb2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@WebServlet(urlPatterns = "/info")
public class HttpInfoServlet extends HttpServlet {
  @Override
  public void init() throws ServletException {
    System.out.println("Initializing...");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    resp.setContentType("text/html");
    PrintWriter writer = resp.getWriter();

    writer.println("<html><body>");
    writer.println("<h1>Http Info<h1>");

    writer.println("<h2>Http Header Names</h2>");
    Enumeration<String> names = req.getHeaderNames();
    List<String> headers = new ArrayList();

    for (String element = names.nextElement(); names.hasMoreElements(); element = names.nextElement()){
      String header = req.getHeader(element);
      headers.add(element + ": " + header);
    }

    String htmlList = generateHtmlList(headers);
    writer.println(htmlList);

    writer.println("<h2>Protocol</h2>");
    String protocol = req.getProtocol();
    writer.println("<p>" + protocol + "</p>");

    writer.println("<h2>HTTP Method</h2>");
    String method = req.getMethod();
    writer.println("<p>" + method + "</p>");

    writer.println("<h2>Request URI</h2>");
    String uri = req.getRequestURI();
    writer.println("<p>" + uri + "</p>");

    writer.println("<h2>Path Info</h2>");
    String pathInfo = req.getPathInfo();
    writer.println("<p>" + pathInfo + "</p>");

    writer.println("</html></body>");



  }

  private String generateHtmlList(List<String> list) {
    StringBuilder htmlList = new StringBuilder("<ol>");

    for (String element : list) {
      htmlList.append("<li>").append(element).append("</li>");
    }
    htmlList.append("</ol>");

    return htmlList.toString();
  }
}
