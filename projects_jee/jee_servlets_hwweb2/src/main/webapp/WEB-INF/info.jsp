<!DOCTYPE html>
<%@ page import = "java.util.*" %>
<html>
    <body>
        <h1>HTTP Info</h1>

        <h2>Header Names</h2>
        <ol>
        <%
            Enumeration<String> names = request.getHeaderNames();
            List<String> headers = new ArrayList();

            for (String element = names.nextElement(); names.hasMoreElements(); element = names.nextElement()){
                String header = request.getHeader(element);
                out.println("<li>" + element + ": " + header + "</li>");
            }
        %>
        </ol>

        <h2>Protocol</h2>
        <%=out.println(request.getProtocol())%>

        <h2>HTTP Method</h2>
        <%=out.println(request.getMethod())%>

        <h2>Request URI</h2>
        <%=request.getRequestURI()%>

        <h2>Path Info</h2>
        <%=request.getPathInfo()%>
    </body>
</html>