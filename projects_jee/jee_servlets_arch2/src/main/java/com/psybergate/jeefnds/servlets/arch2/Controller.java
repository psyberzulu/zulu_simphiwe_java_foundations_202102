package com.psybergate.jeefnds.servlets.arch2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface Controller {
  void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException;
}
