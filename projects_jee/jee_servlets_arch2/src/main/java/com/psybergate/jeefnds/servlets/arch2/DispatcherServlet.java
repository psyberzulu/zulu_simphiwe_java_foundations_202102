package com.psybergate.jeefnds.servlets.arch2;

import org.omg.CORBA.COMM_FAILURE;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet(urlPatterns = {"/"})
public class DispatcherServlet extends HttpServlet {

  private static final Map<String, Controller> CONTROLLERS = new HashMap<>();

  @Override
  public void init() throws ServletException {
    System.out.println("Initializing...");
    CONTROLLERS.put("/helloworld", new HelloWorldController());
    CONTROLLERS.put("/getcurrentdate", new GetCurrentDateController());
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println("In doGet version 2.0");
    String path = req.getServletPath();

    System.out.println("path = " + path);
    Controller controller = getController(path);
    controller.execute(req, resp);
  }

  private Controller getController(String path) {
    return CONTROLLERS.get(path);
  }
}
