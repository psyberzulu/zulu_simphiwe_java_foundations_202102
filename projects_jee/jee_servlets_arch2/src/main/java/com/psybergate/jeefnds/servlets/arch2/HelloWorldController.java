package com.psybergate.jeefnds.servlets.arch2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloWorldController implements Controller {
  @Override
  public void execute(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.setContentType("text/html");
    String name = req.getParameter("name");

    System.out.println("Hello world running...");

    PrintWriter writer = resp.getWriter();
    writer.println("<html><body>");
    writer.println("<h1>Hello " + name + "</h1>");
    writer.println("</body><html>");

  }
}
