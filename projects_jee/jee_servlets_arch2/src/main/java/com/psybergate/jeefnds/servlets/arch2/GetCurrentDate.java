package com.psybergate.jeefnds.servlets.arch2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;

public class GetCurrentDate {
  public void getCurrentDate(HttpServletRequest req,
                                    HttpServletResponse resp) throws IOException {
    resp.setContentType("text/html");
    System.out.println("Get current date running...");

    PrintWriter writer = resp.getWriter();
    writer.println("<html><body>");
    writer.println("<p>" + LocalDate.now() + "<p>");
    writer.println("</html></body>");

  }
}
