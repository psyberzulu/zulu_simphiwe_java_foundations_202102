package com.psybergate.jeefnds.servlets.arch2;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloWorld {
  public void helloWorld(HttpServletRequest req,
                                       HttpServletResponse resp) throws IOException {
    resp.setContentType("text/html");

    System.out.println("Hello world running...");

    PrintWriter writer = resp.getWriter();
    writer.println("<html><body>");
    writer.println("<h1>Hello World</h1>");
    writer.println("</body><html>");

  }
}
