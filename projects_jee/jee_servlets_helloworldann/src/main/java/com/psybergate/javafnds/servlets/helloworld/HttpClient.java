package com.psybergate.javafnds.servlets.helloworld;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class HttpClient {
  public static void main(String[] args) throws IOException {
    URL url =
        new URL("http://localhost:8080/jee_servlet_helloworldann/helloworld");
    URLConnection urlConnection = url.openConnection();

    BufferedReader bufferedReader =
        new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

    String line = bufferedReader.readLine();
    while (line != null) {
      System.out.println(line);
      line = bufferedReader.readLine();
    }
  }
}
