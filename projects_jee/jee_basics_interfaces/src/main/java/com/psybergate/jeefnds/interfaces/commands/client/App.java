package com.psybergate.jeefnds.interfaces.commands.client;

import com.psybergate.jeefnds.interfaces.commands.client.commands.Factorial;
import com.psybergate.jeefnds.interfaces.commands.client.commands.SumNumbers;
import com.psybergate.jeefnds.interfaces.commands.standard.*;
import com.psybergate.jeefnds.interfaces.commands.vendor.VendorCommandEngine;

public class App {
  public static void main(String[] args) {
    CommandEngine commandEngine = new VendorCommandEngine();

    CommandRequest<Integer> request = new DefaultCommandRequest<>(new Factorial());
    request.setParameter(6);
    CommandResponse response = commandEngine.processCommand(request);
    System.out.println(response.getResponse());
  }
}
