package com.psybergate.jeefnds.interfaces.commands.standard;

public interface CommandResponse<T> {

  T getResponse();
}
