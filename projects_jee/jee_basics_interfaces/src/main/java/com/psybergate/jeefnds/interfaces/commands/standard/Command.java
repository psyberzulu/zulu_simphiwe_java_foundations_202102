package com.psybergate.jeefnds.interfaces.commands.standard;

public interface Command {
  String commandName();

  CommandResponse execute(CommandRequest request);
}
