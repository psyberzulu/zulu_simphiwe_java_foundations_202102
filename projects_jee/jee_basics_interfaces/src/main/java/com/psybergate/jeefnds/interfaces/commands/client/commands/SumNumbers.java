package com.psybergate.jeefnds.interfaces.commands.client.commands;

import com.psybergate.jeefnds.interfaces.commands.standard.Command;
import com.psybergate.jeefnds.interfaces.commands.standard.CommandRequest;
import com.psybergate.jeefnds.interfaces.commands.standard.CommandResponse;
import com.psybergate.jeefnds.interfaces.commands.standard.DefaultCommandResponse;

import java.util.List;

public class SumNumbers implements Command {
  @Override
  public String commandName() {
    return "Sum Numbers";
  }

  @Override
  public CommandResponse<Integer> execute(CommandRequest request) {
    List<Integer> numbers = ((List<Integer>) request.getParameter());
    int sum = 0;
    for (Integer number : numbers) {
      sum += number;
    }
    return new DefaultCommandResponse(sum);
  }
}
