package com.psybergate.jeefnds.interfaces.commands.client.commands;

import com.psybergate.jeefnds.interfaces.commands.standard.Command;
import com.psybergate.jeefnds.interfaces.commands.standard.CommandRequest;
import com.psybergate.jeefnds.interfaces.commands.standard.CommandResponse;
import com.psybergate.jeefnds.interfaces.commands.standard.DefaultCommandResponse;

public class Factorial implements Command {
  @Override
  public String commandName() {
    return "Factorial";
  }

  @Override
  public CommandResponse execute(CommandRequest request) {
    int num = (int) request.getParameter();
    int prod = 1;
    for (int i = 1; i <= num; i++) {
      prod *= i;
    }
    return new DefaultCommandResponse(prod);
  }
}
