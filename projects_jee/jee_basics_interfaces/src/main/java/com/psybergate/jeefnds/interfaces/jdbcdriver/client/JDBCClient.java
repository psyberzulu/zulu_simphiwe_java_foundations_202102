package com.psybergate.jeefnds.interfaces.jdbcdriver.client;

import java.sql.*;

public class JDBCClient {
  public static void main(String[] args) throws ClassNotFoundException, SQLException {
    Class
        .forName("com.psybergate.jeefnds.interfaces.jdbcdriver.vendor.MyDriver");
    Connection connection = DriverManager.getConnection("MyDatabase");
    Statement statement = connection.createStatement();
    ResultSet resultSet = statement.executeQuery("query");
  }
}
