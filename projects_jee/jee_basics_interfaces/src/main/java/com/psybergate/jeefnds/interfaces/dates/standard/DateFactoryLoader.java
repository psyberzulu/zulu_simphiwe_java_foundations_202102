package com.psybergate.jeefnds.interfaces.dates.standard;

import java.io.InputStream;
import java.util.Properties;

public class DateFactoryLoader {

  public static DateFactory dateFactory;

  public static DateFactory getDateFactory() {

    if (dateFactory != null) {
      return dateFactory;
    }

    try (InputStream is =
             DateFactoryLoader.class.getClassLoader().getResourceAsStream("date" +
                 ".properties")) {
      Properties properties = new Properties();
      properties.load(is);
      String factory = properties.getProperty("date.factory");
      System.out.println("factory = " + factory);

      dateFactory =
          (DateFactory) Class.forName(factory).newInstance();

      return dateFactory;
    } catch (Exception e) {
      throw new RuntimeException("Error loading date factory class " +
          "dynamically", e);
    }

  }
}
