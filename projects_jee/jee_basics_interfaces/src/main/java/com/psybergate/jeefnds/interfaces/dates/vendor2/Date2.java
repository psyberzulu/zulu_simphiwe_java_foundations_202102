package com.psybergate.jeefnds.interfaces.dates.vendor2;

import com.psybergate.jeefnds.interfaces.dates.standard.Date;

public class Date2 implements Date {
  private int day;

  private int month;

  private int year;

  public Date2(int day, int month, int year) {
    this.day = day;
    this.month = month;
    this.year = year;
  }

  @Override
  public int getDay() {
    return this.day;
  }

  @Override
  public int getMonth() {
    return this.month;
  }

  @Override
  public int getYear() {
    return this.year;
  }
}
