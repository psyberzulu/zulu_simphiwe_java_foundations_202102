package com.psybergate.jeefnds.interfaces.commands.standard;

public interface CommandRequest <T> {
  Command getCommand();

  void setParameter(T parameter);

  T getParameter();
}
