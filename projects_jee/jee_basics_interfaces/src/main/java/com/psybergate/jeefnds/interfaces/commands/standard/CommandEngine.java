package com.psybergate.jeefnds.interfaces.commands.standard;

public interface CommandEngine {
  void start();

  CommandResponse processCommand(CommandRequest command);
}
