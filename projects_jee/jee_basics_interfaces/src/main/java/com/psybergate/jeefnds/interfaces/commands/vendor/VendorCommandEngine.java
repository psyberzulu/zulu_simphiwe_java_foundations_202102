package com.psybergate.jeefnds.interfaces.commands.vendor;

import com.psybergate.jeefnds.interfaces.commands.standard.Command;
import com.psybergate.jeefnds.interfaces.commands.standard.CommandEngine;
import com.psybergate.jeefnds.interfaces.commands.standard.CommandRequest;
import com.psybergate.jeefnds.interfaces.commands.standard.CommandResponse;

public class VendorCommandEngine implements CommandEngine {
  @Override
  public void start() {
    System.out.println("Command Engine Started...");
  }

  @Override
  public CommandResponse processCommand(CommandRequest request) {
    Command command = request.getCommand();
    CommandResponse response = command.execute(request);
    return response;
  }
}
