package com.psybergate.jeefnds.interfaces.commands.standard;

public class DefaultCommandRequest<T> implements CommandRequest<T> {
  private final Command command;

  private T parameter;

  public DefaultCommandRequest(Command command) {
    this.command = command;
  }

  @Override
  public Command getCommand() {
    return command;
  }

  @Override
  public void setParameter(T parameter) {
    this.parameter = parameter;
  }

  @Override
  public T getParameter() {
    return this.parameter;
  }
}
