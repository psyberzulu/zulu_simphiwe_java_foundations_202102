package com.psybergate.jeefnds.interfaces.dates.vendor2;

import com.psybergate.jeefnds.interfaces.dates.standard.Date;
import com.psybergate.jeefnds.interfaces.dates.standard.DateFactory;
import com.psybergate.jeefnds.interfaces.dates.standard.InvalidDateException;

public class DateFactory2 implements DateFactory {
  @Override
  public Date createDate(int year, int month, int day) throws InvalidDateException {
    if (month < 1 || month > 12) {
      throw new InvalidDateException();
    }

    if (day < 1 || day > 31) {
      throw new InvalidDateException();
    }

    if (month == 2 && day > 28) {
      throw new InvalidDateException();
    }

    if (month > 2 && month % 2 == 0 && day > 30) {
      throw new InvalidDateException();
    }

    return new Date2(day, month, year);
  }
}
