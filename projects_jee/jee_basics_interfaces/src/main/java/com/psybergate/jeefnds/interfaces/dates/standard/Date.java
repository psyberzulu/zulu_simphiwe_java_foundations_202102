package com.psybergate.jeefnds.interfaces.dates.standard;

public interface Date {
  int getDay();

  int getMonth();

  int getYear();
}
