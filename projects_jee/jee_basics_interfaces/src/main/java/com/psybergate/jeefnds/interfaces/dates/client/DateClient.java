package com.psybergate.jeefnds.interfaces.dates.client;

import com.psybergate.jeefnds.interfaces.dates.standard.Date;
import com.psybergate.jeefnds.interfaces.dates.standard.DateFactory;
import com.psybergate.jeefnds.interfaces.dates.standard.DateFactoryLoader;
import com.psybergate.jeefnds.interfaces.dates.standard.InvalidDateException;

public class DateClient {
  public static void main(String[] args) {
    DateFactory dateFactory = DateFactoryLoader.getDateFactory();
    System.out.println(dateFactory.getClass());

    Date date = null;
    try {
      date = dateFactory.createDate(2021, 10, 11);
      System.out.println(date.getClass());
    } catch (InvalidDateException e) {
      e.printStackTrace();
    }
  }
}
