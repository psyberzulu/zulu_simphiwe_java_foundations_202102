package com.psybergate.jeefnds.interfaces.dates.standard;

/**
 * @since 29/06/2021
 */
public interface DateFactory {
  Date createDate(int year, int month, int day) throws InvalidDateException;
}
