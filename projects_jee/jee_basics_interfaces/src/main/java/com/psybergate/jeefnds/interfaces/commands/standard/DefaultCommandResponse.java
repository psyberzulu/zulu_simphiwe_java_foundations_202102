package com.psybergate.jeefnds.interfaces.commands.standard;

public class DefaultCommandResponse<T> implements CommandResponse<T>{
  private T response;

  public DefaultCommandResponse(T response) {
    this.response = response;
  }

  @Override
  public T getResponse() {
    return this.response;
  }
}
