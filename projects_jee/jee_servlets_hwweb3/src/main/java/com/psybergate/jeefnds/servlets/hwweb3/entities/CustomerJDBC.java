package com.psybergate.jeefnds.servlets.hwweb3.entities;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class CustomerJDBC {
  private static final String DATABASE_URL = "jdbc:postgresql://localhost" +
      ":5432/";

  private static final String USERNAME = "postgres";

  private static final String PASSWORD = "admin";

  private Connection connection;

  private String databaseName;

  public CustomerJDBC(String databaseName) {
    this.databaseName = databaseName;
  }

  public void connect() throws ClassNotFoundException, SQLException {
    try {
      Class.forName("org.postgresql.Driver");
      String url = DATABASE_URL + databaseName;
      connection = DriverManager.getConnection(url, USERNAME,
          PASSWORD);
      System.out.println("Database successfully opened");
    } catch (SQLException sqlException) {
      if (sqlException.getErrorCode() ==  0) {
        this.createDatabase();
      } else {
        throw sqlException;
      }
    }
  }

  private void createDatabase() throws SQLException, ClassNotFoundException {
    Class.forName("org.postgresql.Driver");
    this.connection = DriverManager.getConnection(DATABASE_URL,
        USERNAME, PASSWORD);

    if (this.connection != null) {
      Statement statement = this.connection.createStatement();
      String query = "create database " + this.databaseName + ";";
      statement.executeUpdate(query);
      statement.close();
    }
  }

  public void executeQuery(String query) throws SQLException {
    Statement statement = this.connection.createStatement();
    statement.executeUpdate(query);
    statement.close();
  }

  public void close() throws SQLException {
    this.connection.close();
    System.out.println("Database closed successfully...");
  }
}


