package com.psybergate.jeefnds.servlets.hwweb3.controller;

import com.psybergate.jeefnds.servlets.hwweb3.entities.Customer;
import com.psybergate.jeefnds.servlets.hwweb3.entities.CustomerJDBC;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CustomerController {
  public void addCustomer(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    Long customerid = Long.parseLong(req.getParameter("customerid"));
    String name = req.getParameter("name");
    String surname = req.getParameter("surname");
    String dateofbirth = req.getParameter("dateofbirth");

    Customer customer = new Customer(customerid, name, surname, dateofbirth);
    customer.saveCustomer();

    RequestDispatcher dispatcher =
        req.getRequestDispatcher("/WEB-INF/success.jsp");
    dispatcher.forward(req, resp);
  }

  public void showCustomerPage(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    RequestDispatcher dispatcher = req.getRequestDispatcher("/WEB-INF" +
        "/customer.jsp");
    dispatcher.forward(req, resp);
  }
}
