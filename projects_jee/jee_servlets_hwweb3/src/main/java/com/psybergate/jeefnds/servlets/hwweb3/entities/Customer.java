package com.psybergate.jeefnds.servlets.hwweb3.entities;

import java.sql.SQLException;

public class Customer {
  private long customerId;
  private String name;
  private String surname;
  private String dateofbirth;

  public Customer(long customerId, String name, String surname, String dateofbirth) {
    this.customerId = customerId;
    this.name = name;
    this.surname = surname;
    this.dateofbirth = dateofbirth;
  }

  public void saveCustomer() {
    CustomerJDBC jdbc = new CustomerJDBC("jee_servlets_hwweb3");
    try {
      jdbc.connect();
      String query = "insert into customer(customer_id, name, surname, " +
          "date_of_birth) values (" + customerId + ", '" + name + "', '" + surname + "', '" + dateofbirth + "')";
      jdbc.executeQuery(query);
      jdbc.close();
    } catch (Exception e) {
      throw new RuntimeException(e.getMessage(), e);
    }

  }
}
