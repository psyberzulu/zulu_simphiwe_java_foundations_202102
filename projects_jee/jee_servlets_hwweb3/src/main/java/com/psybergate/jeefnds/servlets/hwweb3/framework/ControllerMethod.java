package com.psybergate.jeefnds.servlets.hwweb3.framework;

import java.lang.reflect.Method;

public class ControllerMethod {
  private Object instance;

  private Method method;

  public ControllerMethod(Object instance, Method method) {
    this.instance = instance;
    this.method = method;
  }

  public Object getInstance() {
    return instance;
  }

  public Method getMethod() {
    return method;
  }

}
