package com.psybergate.jeefnds.servlets.hwweb3.framework;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet(urlPatterns = {"/dispatcher/*"})
public class DispatcherServlet extends HttpServlet {

  private static final Map<String, ControllerMethod> METHODS = new HashMap<>();

  @Override
  public void init() throws ServletException {
    System.out.println("Initialising");
    loadControllers();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println("doGet...");
    executeController(req, resp);
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    System.out.println("doPost...");
    executeController(req, resp);

  }

  private void executeController(HttpServletRequest req, HttpServletResponse resp) {
    String path = req.getPathInfo();
    path = path.replace("/dispatcher", "");
    System.out.println("Path: " + path);
    ControllerMethod controller = METHODS.get(path);

    if (controller == null) {
      throw new RuntimeException("No controller with requested path: " + path);
    }
    System.out.println("Method: " + controller.getMethod().getName());

    try {
      Method method = controller.getMethod();
      method.invoke(method.getDeclaringClass().newInstance(), req, resp);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private void loadControllers() {
    try {
      InputStream is =
          Thread.currentThread().getContextClassLoader().getResourceAsStream(
              "controller.properties");
      Properties properties = new Properties();
      properties.load(is);

      for (Map.Entry entry : properties
          .entrySet()) {
        String[] strings = entry.getValue().toString().split("#");
        Class<?> clazz = Class.forName(strings[0]);
        Method method = clazz.getDeclaredMethod(strings[1],
            HttpServletRequest.class, HttpServletResponse.class);

        ControllerMethod controller =
            new ControllerMethod(clazz.newInstance(), method);

        METHODS.put("/" + entry.getKey().toString(), controller);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
