package com.psybergate.jeefnds.servlets.helloworld;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import java.io.IOException;
import java.io.PrintWriter;

public class First extends HttpServlet {
  ServletConfig config = null;

  @Override
  public void init(ServletConfig servletConfig) throws ServletException {
    this.config = servletConfig;
    System.out.println("Servlet is initialized");
  }

  public void doGet(ServletRequest request, ServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");

    PrintWriter writer = response.getWriter();
    writer.println("<html><body>");
    writer.println("<b>Hello World</b>");
    writer.println("</body><html>");

  }

  @Override
  public ServletConfig getServletConfig() {
    return this.config;
  }

  @Override
  public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {

  }

  @Override
  public String getServletInfo() {
    return "copyright 2021-1010";
  }

  @Override
  public void destroy() {
    System.out.println("Servlet has been destroyed");
  }
}
