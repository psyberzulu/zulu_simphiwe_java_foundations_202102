package com.psybergate.jeefnds.servlets.arch3.v2.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class GreetingController {
  public void helloWorld(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.setContentType("text/html");
    String name = req.getParameter("name");

    System.out.println("Hello world running...");

    PrintWriter writer = resp.getWriter();
    writer.println("<html><body>");
    writer.println("<h1>Hello " + name + "</h1>");
    writer.println("</body><html>");

  }

  public void goodbyeWorld(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    resp.setContentType("text/html");
    String name = req.getParameter("name");

    System.out.println("Goodbye world running...");

    PrintWriter writer = resp.getWriter();
    writer.println("<html><body>");
    writer.println("<h1>Goodbye " + name + "</h1>");
    writer.println("</body><html>");

  }
}
