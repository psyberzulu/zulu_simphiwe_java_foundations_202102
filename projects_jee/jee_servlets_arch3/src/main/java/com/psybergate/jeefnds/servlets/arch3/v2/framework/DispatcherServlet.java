package com.psybergate.jeefnds.servlets.arch3.v2.framework;

import com.psybergate.jeefnds.servlets.arch3.v2.controller.DateController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

@WebServlet(urlPatterns = {"/*"})
public class DispatcherServlet extends HttpServlet {

  private static final Map<String, Method> METHODS = new HashMap<>();

  @Override
  public void init() throws ServletException {
    System.out.println("Initialising");
    loadControllers();
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String path = req.getPathInfo();
    Method method = METHODS.get(path);

    System.out.println("Path: " + path);

    if (method == null) {
      throw new RuntimeException("No controller with requested path: " + path);
    }
    System.out.println("Method: " + method.getName());

    try {

      method.invoke(method.getDeclaringClass().newInstance(), req, resp);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  private void loadControllers() {
    try {
      InputStream is =
          Thread.currentThread().getContextClassLoader().getResourceAsStream(
              "controller.properties");
      Properties properties = new Properties();
      properties.load(is);

      for (Map.Entry entry : properties
          .entrySet()) {
        String[] strings = entry.getValue().toString().split("#");
        Class<?> clazz = Class.forName(strings[0]);
        Method method = clazz.getDeclaredMethod(strings[1],
            HttpServletRequest.class, HttpServletResponse.class);

        METHODS.put("/" + entry.getKey().toString(), method);
      }
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
