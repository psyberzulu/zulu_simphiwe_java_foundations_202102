//package com.psybergate.jeefnds.servlets.arch3.v1.framework;
//
//import com.psybergate.jeefnds.servlets.arch3.v1.controller.DateController;
//import com.psybergate.jeefnds.servlets.arch3.v1.controller.GreetingController;
//
//import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//
////@WebServlet(urlPatterns = {"/helloworld", "/goodbyeworld", "/getcurrentdate", "/gettomorrowsdate"})
//public class DispatcherServlet extends HttpServlet {
//  @Override
//  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//    String path = req.getServletPath();
//    System.out.println("Dispatcher...");
//
//    if (path.equals("/goodbyeworld")) {
//      GreetingController controller = new GreetingController();
//      controller.goodbyeWorld(req, resp);
//    } else if (path.equals("/helloworld")) {
//      GreetingController controller = new GreetingController();
//      controller.helloWorld(req, resp);
//    } else if (path.equals("/getcurrentdate")) {
//      DateController controller = new DateController();
//      controller.getCurrentDate(req, resp);
//    } else if (path.equals("/gettomorrowsdate")) {
//      DateController controller = new DateController();
//      controller.getTomorrowsDate(req, resp);
//    }
//  }
//}