package com.psybergate.javafnds;

public class Lockers {
    private int[] lockers;
    private int n;

    public Lockers() {
        this.n = 1000;
        this.initLockers();
    }

    public Lockers(int n) {
        this.n = n;
        this.initLockers();
    }

    private void initLockers() {
        this.lockers = new int[this.n];
        for (int i = 0; i < this.n; i++) {
            this.lockers[i] = 0;
        }
    }

    public void openLockersBF() {
        for (int i = 1; i < this.n + 1; i++) {
            for (int j = i; j < this.n + 1; j += i) {
                if (this.lockers[j - 1] == 0) {
                    this.lockers[j - 1] = 1;
                } else {
                    this.lockers[j - 1] = 0;
                }
            }
        }
        printOpenLockers();
    }

    public void printOpenLockers() {
        String line = "Open Lockers:";
        int total = 0;
        for (int i = 0; i < this.n; i++) {
            if (this.lockers[i] == 1) {
                line += " " + (i + 1);
                total++;
            }
        }
        System.out.println(line);
        System.out.println("Total number of open lockers: " + total);
    }

}