// package homework_2;

public class Primes {
    private int n;
    private int[] arr;
    // private Timer t;

    public Primes() {
        this.n = 1000;
        this.init_arr();
        // this.t = new Timer();
    }

    public Primes(int n) {
        this.n = n;
        this.init_arr();
        // this.t = new Timer();
    }

    private void init_arr() {
        this.arr = new int[this.n];

        for (int i = 0; i < this.n; i++) {
            this.arr[i] = 1;
        }
    }

    public void getPrimes() {
        long start = System.nanoTime();
        this.arr[0] = 0;
        for (int i = 2; i < this.n + 1; i++) {
            for (int j =  2 * i; j < this.n + 1; j += i) {
                if (this.arr[j - 1] == 1) {
                    this.arr[j - 1] = 0;
                }
            }
        }
        
        long timeElapsed = System.nanoTime() - start;
        this.printPrimes();
        this.printPerformance(timeElapsed);
    }

    public void printPrimes() {
        String line = "Prime numbers:";
        int total = 0;

        for (int i = 0; i < this.n; i++) {
            if (this.arr[i] == 1) {
                // line += " " + (i + 1);
                total++;
            }
        }

        // System.out.println(line);
        System.out.println("Total number of prime numbers: " + total);
    }

    public void printPerformance(long timeElapsed) {
        System.out.println("Time elapsed (in nanoseconds) for input size " + this.n + ": " + timeElapsed / 1000000);
    }

    // public void printPerformance() {
    //     System.out.println("Time elapsed (in milliseconds): " + this.t.getLastTimeElapsed() / 1000000);
    // }
}