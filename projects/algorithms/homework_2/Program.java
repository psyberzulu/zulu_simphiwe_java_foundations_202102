// package homework_2;
import java.lang.Math;

public class Program {
    public static void main(String[] args) {

        for (int i = 0; i < 3; i++) {
            int size = 20000 * (int) Math.pow(10, i);
            Primes p = new Primes(size);
            p.getPrimes();
        }
    }
}