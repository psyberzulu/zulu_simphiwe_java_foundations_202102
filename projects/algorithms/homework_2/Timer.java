package homework_2;

public class Timer {
    private long startTime;
    private long lastTimeElapsed;

    public Timer() {
        this.startTime = System.nanoTime();
        this.lastTimeElapsed = (long) 0.0;
    }

    public long getLastTimeElapsed() {
        return this.lastTimeElapsed;
    }

    public void startTimer() {
        this.startTime = System.nanoTime();
    }

    public long stopTimer() {
        this.lastTimeElapsed = this.startTime - System.nanoTime();
        return this.lastTimeElapsed;
    }
}