package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public class Money {
    public static void main(String[] args) {
        Money hundredUSD = new Money("USD", 100);
        Money fiftyUSD = new Money("USD", 50);
        Money twentyUSD = new Money("USD", 20);
        Money tenUSD = new Money("USD", 10);
        Money fiveUSD = new Money("USD", 5);
        Money oneUSD = new Money("USD", 1);

        double total = hundredUSD.add(fiftyUSD) + twentyUSD.add(tenUSD) + fiveUSD.add(oneUSD);
        System.out.println("total = " + total);

    }

    private final String currency;
    private final BigDecimal value;

    public Money(String currency, double value) {
        this.currency = currency;
        this.value = new BigDecimal(value);
    }

//    public Money(BigDecimal value) {
//        this.value = value;
//    }
//
//    public Money(String value) {
//        this(new BigDecimal(value));
//    }
//
//    public Money(double value) {
//        this(String.valueOf(value));
//    }
//
//    public Money(int value) {
//        this(String.valueOf(value));
//    }

    public double getValue() {
        return this.value.doubleValue();
    }

    private double add(Money money) {
        return this.getValue() + money.getValue();
    }

//    public Money add(Money money) {
//        return new Money(this.value.add(money.getValue()));
//    }
//
//    public BigDecimal getValue() {
//        return value;
//    }
}
