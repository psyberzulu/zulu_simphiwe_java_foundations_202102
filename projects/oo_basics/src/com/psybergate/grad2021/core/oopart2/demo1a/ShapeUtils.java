package com.psybergate.grad2021.core.oopart2.demo1a;

import java.util.List;

public class ShapeUtils {
    public static void printShapes(List<Shape> shapes) {
        for (Shape shape : shapes) {
            System.out.println("shape.area() = " + shape.area());
        }
    }
}
