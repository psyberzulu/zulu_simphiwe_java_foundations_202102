package com.psybergate.grad2021.core.oopart2.demo1a;

public class Square extends Shape{
    public static void main(String[] args) {

    }

    protected int length1;

    public Square(int length1) {
        this.length1 = length1;
    }

    @Override
    public int area() {
        return this.length1 * this.length1;
    }
}
