package com.psybergate.grad2021.core.oopart2.ce1a;

public abstract class Account {
    public static void main(String[] args) {
        Account account = new CurrentAccount();
//        The next statement won't work because constructors can't be inherited
//        CurrentAccount current = new Account();
    }


    protected String accountNum;
    protected boolean lock;
    protected double balance;

    public Account() {
        this("00", 0);
//        this.accountNum = "0123456";
//        this.balance = 0;
    }

    public Account(String accountNum) {
        this(accountNum, false, 0);
    }

    public Account(String accountNum, double balance) {
        this(accountNum, false, balance);
    }

    public Account(String accountNum, boolean lock, double balance) {
        this.accountNum = accountNum;
        this.lock = lock;
        this.balance = balance;
    }



    public double getBalance() {
        return this.balance;
    }

    public void withdraw(double amount) {
        if (validateWithdrawal()) {
            this.balance -= amount;
        }
    }

    protected void lock() {
        this.lock = true;
    }

    protected void unlock() {
        this.lock = false;
    }

    protected abstract boolean validateReview();

    protected abstract boolean validateWithdrawal();

    protected boolean isLocked() {
        return this.lock;
    }
}
