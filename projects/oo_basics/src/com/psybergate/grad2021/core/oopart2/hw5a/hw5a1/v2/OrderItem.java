package com.psybergate.grad2021.core.oopart2.hw5a.hw5a1.v2;

public class OrderItem {
    private Product product;
    private int quantity;

    public OrderItem(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public double getTotalPrice() {
        return this.product.getPrice() * this.quantity;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void increaseQuantity(int quantity) {
        this.quantity += quantity;
    }

    public void decreaseQuantity(int quantity) {
        this.quantity += quantity;
    }

    public void print() {
        this.product.printDetails();
        System.out.println("Price (Quantity): R" + OrderUtils.formatPrice(this.product.getPrice()) + String.format(" (%d)", this.quantity));
        System.out.println("Total: R" + OrderUtils.formatPrice(this.getTotalPrice()));
    }
}
