package com.psybergate.grad2021.core.oopart2.ce2a;

public class ExerciseUtils {
    public static void main(String[] args) {
        Exercise e1 = new Exercise(1, "One");
        Exercise e2 = new Exercise(2, "Two");

        e1.print();
        swap(e1, e2);
        e1.print();

    }

    public static void swap(Exercise e1, Exercise e2) {
        Exercise temp = e1;
        e1 = e2;
        e2 = e2;
    }
}
