package com.psybergate.grad2021.core.oopart2.hw4a;

import com.psybergate.grad2021.core.oopart2.demo1a.Square;

import java.util.ArrayList;
import java.util.List;

public class SquareUtils extends ShapeUtils {
    public static void main(String[] args) {
        List shapes = new ArrayList();
        shapes.add(new Square(5));
        shapes.add(new Square(2));
        shapes.add(new Square(9));

//        Next statment compiles since static methods are inherited by children
        SquareUtils.printShapes(shapes);
    }
//    @Override
//    public static void printShapes(List<Shape> shapes) {
//        super.printShapes(shapes);
//    }
}
