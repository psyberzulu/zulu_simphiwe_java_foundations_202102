package com.psybergate.grad2021.core.oopart2.hw5a.hw5a3;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderUtils {
    public static void main(String[] args) {
        Product dreams = new Product("P001", "Dreams", 50000);
        Product nightmares = new Product("P002", "Nightmares", 10000);
        Product fantasies = new Product("P003", "Fantasies", 1000);
        Product talk = new Product("PI004", "Talk", 100);

        Customer customer = new Customer("C101", "Mr Gualo", "gualo@gmail.com", "+4412561235", "1970/02/02", false);
        Customer customer1 = new Customer("C102", "Mr Maleningi", "maleningi@gmail.com", "+2712871235", "1970/05/07", "2017/04/09", true);
        Customer customer2 = new Customer("C103", "Mr Moreki", "moreki@gmail.com", "+2712561235", "1970/04/02", "2010/05/30", true);
        Customer customer3 = new Customer("C104", "Mr Moola", "moreki@gmail.com", "+2712561235", "1970/04/02", "2010/05/30", true);

        OrderItem item1 = new OrderItem(dreams, 3);
        OrderItem item2 = new OrderItem(fantasies, 5);
        OrderItem item3 = new OrderItem(nightmares, 5);
        OrderItem item4 = new OrderItem(talk, 5);
        OrderItem item5 = new OrderItem(dreams, 10);
        OrderItem item6 = new OrderItem(fantasies, 10);
        OrderItem item7 = new OrderItem(nightmares, 10);
        OrderItem item8 = new OrderItem(talk, 10);
        OrderItem item9 = new OrderItem(dreams, 15);

//        Mr Gualo
        Order order = new InternationalOrder("IO001", customer.getYearsServed(), "Spain");
        order.addItem(item1);
        order.addItem(item3);
        order.addItem(item4);
        order.addItem(item2);
        Order order1 = new InternationalOrder("IO002", "Spain");
        order1.addItem(item2);
        order1.addItem(item4);
        order1.addItem(item1);
        order1.addItem(item3);
        order1.addItem(item5);
        order1.addItem(item6);
        order1.addItem(item9);
        customer.placeOrder(order);
        customer.placeOrder(order1);

//        Mr Maleningi
        Order order2 = new LocalOrder("LO001", customer1.getYearsServed(), "Gauteng");
        order2.addItem(item2);
        order2.addItem(item3);
        Order order3 = new LocalOrder("LO002", "Gauteng");
        order3.addItem(item2);
        order3.addItem(item1);
        order3.addItem(item4);
        order3.addItem(item3);
        order3.addItem(item7);
        order3.addItem(item6);
        order3.addItem(item5);
        order3.addItem(item8);
        customer1.placeOrder(order2);
        customer1.placeOrder(order3);

        //        Mr Maleningi
        Order order5 = new LocalOrder("LO001", customer1.getYearsServed(), "KwaZulu-Natal");
        order5.addItem(item2);
        order5.addItem(item3);
        Order order6 = new LocalOrder("LO002", "KwaZulu-Natal");
        order6.addItem(item2);
        order6.addItem(item1);
        customer2.placeOrder(order5);
        customer2.placeOrder(order6);


        List<Customer> customers = new ArrayList();
        customers.add(customer);
        customers.add(customer1);
        customers.add(customer2);

//        printCustomers(customers);
        printCustomersAndOrders(customers);
    }

    private static final String customerSeparator = "===================================================";
    private static final String orderSeparator = "-----------------------------";
    private static final String itemSeparator = "-------------";


    private static LocalDate convertDate(String date) {
        String[] split = date.split("/");
        int day = Integer.parseInt(split[0]);
        int month = Integer.parseInt(split[1]);
        int year = Integer.parseInt(split[2]);
        return LocalDate.of(year, month, day);
    }

    public static double getOrderTotal(List<OrderItem> orderItems) {
        double total = 0;

        for (OrderItem item : orderItems) {
            total = item.getTotalPrice();
        }
        return total;
    }

    public static void printCustomers(List<Customer> customers) {
        for (Customer customer :
                customers) {
            printCustomerSeparatorLine();
            customer.printDetails();
            printCustomerSeparatorLine();
        }
    }

    public static void printCustomerSeparatorLine() {
        System.out.println(customerSeparator);
    }

    public static void printOrderSeparatorLine() {
        System.out.println(orderSeparator);
    }

    public static void printOrderItemSeparatorLine() {
        System.out.println(itemSeparator);
    }

    public static void printCustomersAndOrders(List<Customer> customers) {
        for (Customer customer : customers) {
            printCustomerSeparatorLine();
            customer.printDetails();
            printOrders(customer.getOrders());
            printCustomerSeparatorLine();
        }
    }

    private static void printOrders(List<Order> orders) {
        System.out.println("Orders:");
        for (Order order : orders) {
            printOrderSeparatorLine();
            order.printOrder();
            printOrderSeparatorLine();
        }
        double customersTotal = getCustomersTotal(orders);
        System.out.println("Customer's Total: " + formatFloat(customersTotal));
    }

    private static double getCustomersTotal(List<Order> orders) {
        double total = 0;
        for (Order order : orders) {
            total += order.getTotal();
        }
        return total;
    }

    public static String formatFloat(double amount) {
        return String.format("%1.2f", amount);
    }
}