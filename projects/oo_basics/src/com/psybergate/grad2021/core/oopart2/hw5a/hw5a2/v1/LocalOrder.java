package com.psybergate.grad2021.core.oopart2.hw5a.hw5a2.v1;

public class LocalOrder extends Order {
    private int yearsServed;
    private static final int MIN_DISCOUNT_THRESHOLD = 2;
    private static final int MAX_DISCOUNT_THRESHOLD = 5;
    private static final double MIN_DISCOUNT = 7.5;
    private static final double MAX_DISCOUNT = 12.5;

    public LocalOrder(String orderNum) {
        this(orderNum, 0);
    }



    public LocalOrder(String orderNum, int yearsServed) {
        super(orderNum);
        this.yearsServed = yearsServed;
        this.calculateDiscount();
    }

    @Override
    protected void calculateDiscount() {
        if (this.yearsServed >= MAX_DISCOUNT_THRESHOLD) {
            this.discount = MAX_DISCOUNT;
        } else if (this.yearsServed >= MIN_DISCOUNT_THRESHOLD) {
            this.discount = MIN_DISCOUNT;
        } else {
            this.discount = DEFAULT_DISCOUNT;
        }
    }

    public String getOrderType() {
        return "Local Order";
    }
}
