package com.psybergate.grad2021.core.oopart2.hw2a;

public class Rectangle {
    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(150,100);
        Object r2 = new Rectangle(150,100);

        System.out.println("r1.equals(r2) = " + r1.equals(r2));
        System.out.println("(r1 == r2) = " + (r1 == r2));
    }

    private int length1;
    private int length2;

    public Rectangle(int length1, int length2) {
        this.length1 = length1;
        this.length2 = length2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || !(o instanceof Rectangle)) {
            return false;
        }
        Rectangle r = (Rectangle) o;
        int rLength1 = r.getLength1();
        int rLength2 = r.getLength2();
        boolean out = this.length1 == rLength1 && this.length2 == rLength2;
        out = out || (this.length1 == rLength2 && this.length2 == rLength1);

        return out;
    }

    private int getLength1() {
        return this.length1;
    }

    private int getLength2() {
        return this.length2;
    }
}
