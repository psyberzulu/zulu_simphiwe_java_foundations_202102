package com.psybergate.grad2021.core.oopart2.hw5a.hw5a2.v1;

public class Product {
    private String barcode;
    private String name;
    private double price;

    public Product(String barcode, String name, double price) {
        this.barcode = barcode;
        this.name = name;
        this.price = price;
    }

    public void printDetails() {
        System.out.println("Barcode: " + this.barcode);
        System.out.println("Item: " + this.name);
    }

    public double getPrice() {
        return this.price;
    }
}
