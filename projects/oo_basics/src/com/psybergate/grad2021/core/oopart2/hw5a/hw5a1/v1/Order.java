package com.psybergate.grad2021.core.oopart2.hw5a.hw5a1.v1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;

public class Order {
    protected String orderNum;
    protected LocalDate date;
    protected List<OrderItem> orderItems;


    public Order(String orderNum) {
        this.orderNum = orderNum;
        this.date = LocalDate.now();
        this.orderItems = new ArrayList<OrderItem>();
    }

    public void addItem(Product product, int quantity) {
        OrderItem item = new OrderItem(product, quantity);
        this.addItem(item);
    }

    public void addItem(OrderItem item) {
        this.orderItems.add(item);
    }

    public double getTotal() {
        return OrderUtils.getOrderTotal(orderItems);
    }

    public String getDate() {
        return this.date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
    }

    public void printOrder() {
        printDetails();
        printOrderItems();
        printOrderTotal();
    }

    protected void printOrderTotal() {
        System.out.println("Order Total: R" + OrderUtils.formatPrice(this.getTotal()));
    };

    private void printOrderItems() {
        System.out.println("Items:");
        for (OrderItem item: this.orderItems) {
            OrderUtils.printOrderItemSeparatorLine();
            item.print();
            OrderUtils.printOrderItemSeparatorLine();
        }
    }

    private void printDetails() {
        System.out.println("Order: " + this.orderNum);
        System.out.println("Order Type: " + this.getOrderType());
        System.out.println("Date: " + this.getDate());
    }

    public String getOrderType() {
        return "Order";
    }
}

