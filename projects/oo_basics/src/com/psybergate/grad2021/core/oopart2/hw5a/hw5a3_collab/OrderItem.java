package com.psybergate.grad2021.core.oopart2.hw5a.hw5a3_collab;

public class OrderItem {
    private Product product;
    private int quantity;

    public OrderItem(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public double getTotalPrice() {
        return this.product.getPrice() * this.quantity;
    }

    public int getQuantity() {
        return this.quantity;
    }

    public void print() {
        OrderUtils.printOrderItemSeparatorLine();
        this.product.printDetails();
        System.out.println("Price (Quantity): R" + OrderUtils.formatFloat(this.product.getPrice()) + String.format(" (%d)", this.quantity));
        System.out.println("Total: R" + OrderUtils.formatFloat(this.getTotalPrice()));
        OrderUtils.printOrderItemSeparatorLine();
    }
}
