package com.psybergate.grad2021.core.oopart2.hw5a.hw5a2.v2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderUtils {
    public static void main(String[] args) {
        Product dreams = new Product("P001", "Dreams", 5);
        Product nightmares = new Product("P002", "Nightmares", 1);
        Product fantasies = new Product("P003", "Fantasies", 1);
        Product talk = new Product("PI004", "Talk", 0.5);

        OrderItem item1 = new OrderItem(dreams, 3);
        OrderItem item2 = new OrderItem(fantasies, 5);
        OrderItem item3 = new OrderItem(nightmares, 5);
        OrderItem item4 = new OrderItem(talk, 5);

        Customer customer = new Customer("C101", "Mr Gualo", "gualo@gmail.com", "+4412561235", "1970/02/02", false);
        Customer customer1 = new Customer("C102", "Mr Maleningi", "maleningi@gmail.com", "+2712871235", "1970/05/07", true);
        Customer customer2 = new Customer("C103", "Mr Moreki", "moreki@gmail.com", "+2712561235", "1970/04/02", true);

        Customer.add(customer);
        Customer.add(customer1);
        Customer.add(customer2);

        Order order = new LocalOrder("LO001", customer1);
        order.addItem(item2);
        order.addItem(item3);
        order.addItem(item4);

        Order order1 = new LocalOrder("LO002", customer1);
        order1.addItem(item2);
        order1.addItem(item1);

        Order order2 = new InternationalOrder("IO001", customer, "Spain");
        order2.addItem(item1);
        order2.addItem(item3);

        Order order3 = new InternationalOrder("IO002", customer, "Spain");
        order3.addItem(item1);
        order3.addItem(item3);

        ORDERS.add(order);
        ORDERS.add(order1);
        ORDERS.add(order2);
        ORDERS.add(order3);

        printCustomersAndOrders();
    }
    // Class collections
    private static final List<Order> ORDERS = new ArrayList();

    // Printing constants
    private static final String ORDER_SEPARATOR = "-----------------------------";
    private static final String ITEM_SEPARATOR = "-------------";


    private static LocalDate convertDate(String date) {
        String[] split = date.split("/");
        int day = Integer.parseInt(split[0]);
        int month = Integer.parseInt(split[1]);
        int year = Integer.parseInt(split[2]);
        return LocalDate.of(year, month, day);
    }

    public static double getOrderTotal(List<OrderItem> orderItems) {
        double total = 0;

        for (OrderItem item : orderItems) {
            total = item.getTotalPrice();
        }
        return total;
    }

    public static void printOrderSeparatorLine() {
        System.out.println(ORDER_SEPARATOR);
    }

    public static void printOrderItemSeparatorLine() {
        System.out.println(ITEM_SEPARATOR);
    }

    public static void printCustomerOrders(Customer customer, List<Order> orders) {
        List<Order> customerOrders = getCustomerOrders(customer, orders);
        printOrders(orders);
    }

    public static void printCustomerOrders(Customer customer) {
        printCustomerOrders(customer, ORDERS);
    }

    private static List<Order> getCustomerOrders(Customer customer, List<Order> orders) {
        List<Order> customerOrders = new ArrayList<>();
        for (Order order :
                orders) {
            if (order.isOrderedBy(customer)) {
                customerOrders.add(order);
            }
        }
        return customerOrders;
    }

    private static List<Order> getCustomerOrders(Customer customer) {
        return getCustomerOrders(customer, ORDERS);
    }

    public static void printCustomersAndOrders() {
        printCustomersAndOrders(Customer.getList(), ORDERS);
    }

    public static void printCustomersAndOrders(List<Customer> customers, List<Order> orders) {
        for (Customer customer :
                customers) {
            Customer.printCustomerSeparatorLine();
            printCustomerOrders(customer, orders);
            Customer.printCustomerSeparatorLine();
        }
    }

    private static void printOrders(List<Order> orders) {
        System.out.println("Orders:");
        for (Order order : orders) {
            printOrderSeparatorLine();
            order.printOrder();
            printOrderSeparatorLine();
        }
        double ordersTotal = getOrdersTotal(orders);
        System.out.println("Orders Total: " + formatPrice(ordersTotal));
    }

    private static double getOrdersTotal(List<Order> orders) {
        double total = 0;
        for (Order order : orders) {
            total += order.getTotal();
        }
        return total;
    }

    public static String formatPrice(double amount) {
        return String.format("%1.2f", amount);
    }
}
