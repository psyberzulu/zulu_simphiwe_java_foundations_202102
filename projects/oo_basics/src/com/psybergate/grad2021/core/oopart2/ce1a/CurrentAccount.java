package com.psybergate.grad2021.core.oopart2.ce1a;

public class CurrentAccount extends Account {
    public static final int MAX_OVERDRAFT = 10000;
    private double overdraft;


    public CurrentAccount() {
        // Delegate object construction to another constructor
        this("003", 0, MAX_OVERDRAFT);
    }

    public CurrentAccount(String accountNum, double balance, double overdraft) {
//        parent constructor must either be:
//            - explictly specified in a super() method call
//            - implemented as default constructor in class implementation
        super(accountNum, balance);
        this.overdraft = overdraft;
    }

    @Override
    public boolean validateReview() {
        return this.isOverDrawn();
    }

    @Override
    protected boolean validateWithdrawal() {
        return this.isOverDrawn() && !super.isLocked();
    }

    private boolean isOverDrawn() {
        return overdraft < -1.2 * MAX_OVERDRAFT;
    }
}
