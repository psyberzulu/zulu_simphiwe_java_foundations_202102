package com.psybergate.grad2021.core.oopart2.hw5a.hw5a1.v1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Customer {
    private String customerNum;
    private String name;
    private String email;
    private String phone;
    private LocalDate dob;
    private boolean local;
    private List<Order> orders;

    public Customer(String customerNum, String name, String email, String phone, String dob, boolean local) {
        this(customerNum, name, email, phone, dob, local, new ArrayList());

    }

    public Customer(String customerNum, String name, String email, String phone, String dob, boolean local, List orders) {
        this.customerNum = customerNum;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.dob = convertDate(dob);
        this.local = local;
        this.orders = orders;
    }

    private static LocalDate convertDate(String date) {
        String[] split = date.split("/");
        int year =  Integer.parseInt(split[0]);
        int month =  Integer.parseInt(split[1]);
        int day =  Integer.parseInt(split[2]);
        return LocalDate.of(year, month, day);
    }

    public void printDetails() {
        if (this.local) {
            System.out.println("International Customer");
        } else {
            System.out.println("Local Customer");
        }
        System.out.println("Customer number: " + this.customerNum);
        System.out.println("Name: " + this.name);
    }

    public void placeOrder(Order order) {
        if (this.local && order instanceof LocalOrder) {
            this.orders.add(order);
        } else if (!this.local && order instanceof InternationalOrder) {
            this.orders.add(order);
        }
    }

    public List getOrders() {
        return this.orders;
    }
}
