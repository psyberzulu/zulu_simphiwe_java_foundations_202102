package com.psybergate.grad2021.core.oopart2.demo1a;

public class Rectangle extends Square{
    private int length2;

    public Rectangle(int length1, int length2) {
//        this.Square(length1)
        super(length1);
        this.length2 = length2;
    }

//    Override cannot be applied to constructor
//    @Override
//    public Square(int length1) {
//        this.length1 = length1;
//        this.length2 = length2;
//    }

    @Override
    public int area() {
        return this.length1 * this.length2;
    }
}
