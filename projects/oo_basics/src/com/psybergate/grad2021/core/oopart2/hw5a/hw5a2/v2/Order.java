package com.psybergate.grad2021.core.oopart2.hw5a.hw5a2.v2;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;

public class Order {
    protected String orderNum;
    protected LocalDate orderDate;
    protected Customer customer;
    protected List<OrderItem> orderItems;

    public Order(String orderNum, Customer customer) {
        this.orderNum = orderNum;
        this.customer = customer;
        this.orderItems = new ArrayList<>();
        this.orderDate = LocalDate.now();
    }

    public void addItem(Product product, int quantity) {
        OrderItem item = new OrderItem(product, quantity);
        this.addItem(item);
    }

    public void addItem(OrderItem item) {
        this.orderItems.add(item);
    }

    public void removeItem(OrderItem item) {
        this.orderItems.remove(item);
    }

    public double getTotal() {
        return OrderUtils.getOrderTotal(orderItems);
    }

    public String getOrderDate() {
        return this.orderDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
    }

    public boolean isOrderedBy(Customer customer) {
        return this.equals(customer);
    }

    public void printOrder() {
        printDetails();
        printOrderItems();
        printOrderTotal();
    }

    protected void printOrderTotal() {
        System.out.println("Order Total: R" + OrderUtils.formatPrice(this.getTotal()));
    }

    private void printOrderItems() {
        System.out.println("Items:");
        for (OrderItem item : this.orderItems) {
            OrderUtils.printOrderItemSeparatorLine();
            item.print();
            OrderUtils.printOrderItemSeparatorLine();
        }
    }

    private void printDetails() {
        System.out.println("Order: " + this.orderNum);
        System.out.println("Order Type: " + this.getOrderType());
        System.out.println("Date: " + this.getOrderDate());
    }

    public String getOrderType() {
        return "Order";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (null == o) {
            return false;
        }

        if (this.getClass() != o.getClass()) {
            return false;
        }

        Order order = (Order) o;
        return this.orderNum.equals(order.orderNum);
    }
}

