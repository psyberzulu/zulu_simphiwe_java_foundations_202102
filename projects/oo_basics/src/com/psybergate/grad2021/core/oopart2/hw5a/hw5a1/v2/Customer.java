package com.psybergate.grad2021.core.oopart2.hw5a.hw5a1.v2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Customer {
    private static final List<Customer> CUSTOMERS = new ArrayList();
    private static final String CUSTOMER_SEPARATOR = "===================================================";


    private String customerNum;
    private String name;
    private String email;
    private String phone;
    private LocalDate dob;
    private boolean local;

    public Customer(String customerNum, String name, String email, String phone, LocalDate dob, boolean local) {
        this.customerNum = customerNum;
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.dob = dob;
        this.local = local;
    }

    public Customer(String customerNum) {
        this(customerNum, "N/A", "N/A", "N/A", LocalDate.now(), true);
    }

    public Customer(String customerNum, String name, String email, String phone, String dob, boolean local) {
        this(customerNum, name, email, phone, convertDate(dob), local);
    }

    private static LocalDate convertDate(String date) {
        String[] split = date.split("/");
        int year = Integer.parseInt(split[0]);
        int month = Integer.parseInt(split[1]);
        int day = Integer.parseInt(split[2]);
        return LocalDate.of(year, month, day);
    }

    public static void printCustomers(List<Customer> customers) {
        for (Customer customer :
                customers) {
            printCustomerSeparatorLine();
            customer.printDetails();
            printCustomerSeparatorLine();
        }
    }

    public static void printCustomerSeparatorLine() {
        System.out.println(CUSTOMER_SEPARATOR);
    }

    public static void add(Customer customer) {
        CUSTOMERS.add(customer);
    }

    public static List<Customer> getList() {
        return CUSTOMERS;
    }

    public void printDetails() {
        if (this.local) {
            System.out.println("International Customer");
        } else {
            System.out.println("Local Customer");
        }
        System.out.println("Customer number: " + this.customerNum);
        System.out.println("Name: " + this.name);
        System.out.println("Email: " + this.email);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (this.getClass() != o.getClass()) {
            return false;
        }

        Customer customer = (Customer) o;
        return customer.customerNum.equals(customer.customerNum);
    }

    public String getCustomerNum() {
        return this.customerNum;
    }

    public static Customer getCustomer(String customerNum) {
        Customer customer = new Customer(customerNum);
        for (Customer c :
                CUSTOMERS) {
            if (c.equals(customer)) {
                return c;
            }
        }
        return customer;
    }
}
