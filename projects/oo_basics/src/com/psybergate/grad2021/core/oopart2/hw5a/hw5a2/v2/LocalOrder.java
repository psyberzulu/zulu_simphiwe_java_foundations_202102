package com.psybergate.grad2021.core.oopart2.hw5a.hw5a2.v2;

public class LocalOrder extends Order {
    private int discount;
    private static final int DEFAULT_DISCOUNT = 10;
    public static final String LOCAL_ORDER = "Local Order";

    public LocalOrder(String orderNum, Customer customer) {
        this(orderNum, customer, DEFAULT_DISCOUNT);
    }

    public LocalOrder(String orderNum, Customer customer, int discount) {
        super(orderNum, customer);
        this.discount = discount;
    }

    @Override
    public double getTotal() {
        return (1.0 - this.discount/ 100.0) * super.getTotal();
    }

    public String getOrderType() {
        return LOCAL_ORDER;
    }
}
