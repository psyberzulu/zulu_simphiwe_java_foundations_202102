package com.psybergate.grad2021.core.oopart2.demo2a;

import java.util.Objects;

public class Demo {
    public static void main(String[] args) {
        Demo demo1 = new Demo(1, "1");
        Demo demo2 = new Demo(1, "1");

        System.out.println("(demo1 == demo2) = " + (demo1 == demo2));
        System.out.println("demo1.equals(demo2) = " + demo1.equals(demo2));
        System.out.println("demo1.equals(demo1) = " + demo1.equals(demo1));
        System.out.println("demo1.equals(\"abc\") = " + demo1.equals("abc"));
    }

    private int num;
    private String string;

    public Demo(int num, String string) {
        this.num = num;
        this.string = string;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        }
        if (!o.getClass().equals(this.getClass())) {
            return false;
        }
        Demo d = (Demo) o;
        return this.num == d.num;
    }

}
