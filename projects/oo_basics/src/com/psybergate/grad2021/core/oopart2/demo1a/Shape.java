package com.psybergate.grad2021.core.oopart2.demo1a;

public abstract class Shape {
    public abstract int area();
}
