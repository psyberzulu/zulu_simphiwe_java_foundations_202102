package com.psybergate.grad2021.core.oopart2.hw5a.hw5a1.v1;

public class LocalOrder extends Order{
    private int discount;
    private static final int DEFAULT_DISCOUNT = 10;

    public LocalOrder(String orderNum) {
        this(orderNum, DEFAULT_DISCOUNT);
    }

    public LocalOrder(String orderNum, int discount) {
        super(orderNum);
        this.discount = discount;
    }

    @Override
    public double getTotal() {
        return (1.0 - this.discount/ 100.0) * super.getTotal();
    }

    public String getOrderType() {
        return "Local Order";
    }
}
