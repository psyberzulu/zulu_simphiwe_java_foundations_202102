package com.psybergate.grad2021.core.oopart2.hw5a.hw5a1.v1;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderUtils {
    public static void main(String[] args) {
        Product dreams = new Product("P001", "Dreams", 5);
        Product nightmares = new Product("P002", "Nightmares", 1);
        Product fantasies = new Product("P003", "Fantasies", 1);
        Product talk = new Product("PI004", "Talk", 0.5);

        OrderItem item1 = new OrderItem(dreams, 3);
        OrderItem item2 = new OrderItem(fantasies, 5);
        OrderItem item3 = new OrderItem(nightmares, 5);
        OrderItem item4 = new OrderItem(talk, 5);

        Order order = new LocalOrder("LO001");
        order.addItem(item2);
        order.addItem(item3);
        Order order1 = new LocalOrder("LO002");
        order1.addItem(item2);
        order1.addItem(item1);
        Order order2 = new InternationalOrder("IO001", "Spain");
        order2.addItem(item1);
        order2.addItem(item3);
        Order order3 = new InternationalOrder("IO002", "Spain");
        order3.addItem(item1);
        order3.addItem(item3);

        Customer customer = new Customer("C101", "Mr Gualo", "gualo@gmail.com", "+4412561235", "1970/02/02", false);
        customer.placeOrder(order2);
        customer.placeOrder(order3);
        Customer customer1 = new Customer("C102", "Mr Maleningi", "maleningi@gmail.com", "+2712871235", "1970/05/07", true);
        customer1.placeOrder(order1);
        customer1.placeOrder(order);
        Customer customer2 = new Customer("C103", "Mr Moreki", "moreki@gmail.com", "+2712561235", "1970/04/02", true);
        customer2.placeOrder(order);
        customer2.placeOrder(order1);


        List<Customer> customers = new ArrayList();
        customers.add(customer);
        customers.add(customer1);
        customers.add(customer2);

        printCustomers(customers);
        printCustomersAndOrders(customers);
    }

    private static final String customerSeparator = "===================================================";
    private static final String orderSeparator = "-----------------------------";
    private static final String itemSeparator = "-------------";


    private static LocalDate convertDate(String date) {
        String[] split = date.split("/");
        int day = Integer.parseInt(split[0]);
        int month = Integer.parseInt(split[1]);
        int year = Integer.parseInt(split[2]);
        return LocalDate.of(year, month, day);
    }

    public static double getOrderTotal(List<OrderItem> orderItems) {
        double total = 0;

        for (OrderItem item : orderItems) {
            total = item.getTotalPrice();
        }
        return total;
    }

    public static void printCustomers(List<Customer> customers) {
        for (Customer customer :
                customers) {
            printCustomerSeparatorLine();
            customer.printDetails();
            printCustomerSeparatorLine();
        }
    }

    public static void printCustomerSeparatorLine() {
        System.out.println(customerSeparator);
    }

    public static void printOrderSeparatorLine() {
        System.out.println(orderSeparator);
    }
    public static void printOrderItemSeparatorLine() {
        System.out.println(itemSeparator);
    }

    public static void printCustomersAndOrders(List<Customer> customers) {
        for (Customer customer: customers) {
            printCustomerSeparatorLine();
            customer.printDetails();
            printOrders(customer.getOrders());
            printCustomerSeparatorLine();
        }
    }

    private static void printOrders(List<Order> orders) {
        System.out.println("Orders:");
        for (Order order : orders) {
            printOrderSeparatorLine();
            order.printOrder();
            printOrderSeparatorLine();
        }
        double customersTotal = getCustomersTotal(orders);
        System.out.println("Customer's Total: " + formatPrice(customersTotal));
    }

    private static double getCustomersTotal(List<Order> orders) {
        double total = 0;
        for (Order order : orders) {
            total += order.getTotal();
        }
        return total;
    }

    public static String formatPrice(double amount) {
        return String.format("%1.2f", amount);
    }
}
