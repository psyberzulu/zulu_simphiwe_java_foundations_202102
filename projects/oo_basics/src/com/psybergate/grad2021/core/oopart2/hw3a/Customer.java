package com.psybergate.grad2021.core.oopart2.hw3a;

import java.util.Objects;

public class Customer {
    private String customerNum;
    private String address;
    private String email;
    private String name;
    private double credit = 0;
    private double maxCredit = 10000;

    public Customer() {
        this("new_customer", "customer", "n/a", "customer@email.com");
    }

    public Customer(String customerNum, String name, String address, String email) {
        this.customerNum = customerNum;
        this.address = address;
        this.email = email;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true; // Identity
        if (o == null) return false; // Non-nullabilty test
        if (this.getClass() != o.getClass()) { // Type equivalence
            return false;
        }
        Customer customer = (Customer) o;
        return this.customerNum.equals(customer.customerNum); // State equivalence
    }

    public void makePurchase(double amount) {
        if (this.validatePurchase(amount)) {
            this.credit += amount;
        }
    }

    private boolean validatePurchase(double amount) {
        return this.credit <= (maxCredit - amount);
    }

    public void makePayment(double amount) {
        this.credit -= amount;
    }

    public void printDetails() {
        System.out.println("---------");
        System.out.println("Customer ID: " + this.customerNum);
        System.out.println("Name: " + this.name);
        System.out.println("Address: " + this.address);
        System.out.println("Email: " + this.email);
        System.out.println("---------");
    }
}
