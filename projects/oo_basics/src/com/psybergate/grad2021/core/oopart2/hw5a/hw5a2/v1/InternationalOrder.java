package com.psybergate.grad2021.core.oopart2.hw5a.hw5a2.v1;

public class InternationalOrder extends Order {
    private String country;
    private static final double MIN_DISCOUNT_THRESHOLD = 500000;
    private static final double MAX_DISCOUNT_THRESHOLD = 1000000;
    private static final double MIN_DISCOUNT = 5;
    private static final double MAX_DISCOUNT = 10;

    public InternationalOrder(String orderNum, String country) {
        super(orderNum);
        this.country = country;
    }

    @Override
    protected void calculateDiscount() {
        if (total >= MAX_DISCOUNT_THRESHOLD) {
            this.discount = MAX_DISCOUNT;
        } else if (total >= MIN_DISCOUNT_THRESHOLD) {
            this.discount = MIN_DISCOUNT;
        } else {
            this.discount = DEFAULT_DISCOUNT;
        }
    }

    @Override
    public double getTotal() {
        this.calculateDiscount();
        return super.getTotal();
    }

    public String getOrderType() {
        return "International Order";
    }

    @Override
    public void printOrder() {
        super.printOrder();
        System.out.println("Country: " + country);
    }
}
