package com.psybergate.grad2021.core.oopart2.hw5a.hw5a3;

import com.sun.xml.internal.fastinfoset.util.StringArray;

import java.util.Arrays;

public class LocalOrder extends Order {
    private String province;

    public LocalOrder(String orderNum, String province) {
        super(orderNum);
        this.province = province;
    }

    public LocalOrder(String orderNum, int yearsServed, String province) {
        super(orderNum, yearsServed);
    }

    public String getOrderType() {
        return "Local Order";
    }
}
