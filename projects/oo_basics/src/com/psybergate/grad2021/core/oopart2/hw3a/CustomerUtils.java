package com.psybergate.grad2021.core.oopart2.hw3a;

import com.psybergate.grad2021.core.oopart1.ce1a.Account;

import java.util.ArrayList;
import java.util.List;

public class CustomerUtils {
    public static void main(String[] args) {
        List customers = new ArrayList();
        customers.add(new Customer("C1021", "Mr Gualo", "Rack City", "gualo@gmail.com"));
        customers.add(new Customer("C1022", "Mr Gualo", "Rack City", "gualo@gmail.com"));
        customers.add(new Customer("C1021", "Mr Racks", "Rack City", "racks@gmail.com"));
        customers.add(customers.get(2)); // Previous Customer object
        customers.add(new Customer("C1023", "Mr Racks", "Rack City", "racks@gmail.com"));

        customers = unique(customers);
        printCustomers(customers);
    }

    public static void printCustomers(List<Customer> customers) {
        for (Customer customer : customers) {
            customer.printDetails();
        }
    }

    public static List unique(List<Customer> customers) {
        List unique = new ArrayList();

        for (Customer customer : customers) {
            if (!unique.contains(customer)) {
                unique.add(customer);
            }
        }

        return unique;
    }
}
