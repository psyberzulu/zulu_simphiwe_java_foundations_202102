package com.psybergate.grad2021.core.oopart2.hw5a.hw5a3;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;

public class Order {
    protected static double DEFAULT_DISCOUNT = 0;
    private static double minTotalBasedThreshold = 500000;
    private static double maxTotalBasedThreshold = 1000000;
    private static double minTotalBasedDiscount = 5;
    private static double maxTotalBasedDiscount = 10;
    private static int minTimeBasedThreshold = 2;
    private static int maxTimeBasedThreshold = 5;
    private static double minTimeBasedDiscount = 7.5;
    private static double maxTimeBasedDiscount = 12.5;
    protected String orderNum;
    protected LocalDate orderDate;
    protected List<OrderItem> orderItems;
    protected double discount;
    protected double total;
    protected int yearsServed;


    public Order(String orderNum) {
        this(orderNum, 0);
    }


    public Order(String orderNum, int yearsServed) {
        this.orderNum = orderNum;
        this.yearsServed = yearsServed;
        this.orderDate = LocalDate.now();
        this.orderItems = new ArrayList<OrderItem>();
        this.total = 0;
        this.calculateDiscount();
    }

    public void addItem(Product product, int quantity) {
        OrderItem item = new OrderItem(product, quantity);
        this.addItem(item);
    }

    public void addItem(OrderItem item) {
        this.orderItems.add(item);
        this.total += item.getTotalPrice();
    }

    private void calculateTotal() {
        this.total = OrderUtils.getOrderTotal(orderItems);
    }

    public double getTotal() {
        this.calculateDiscount();
        return this.total * (1.0 - this.discount / 100.0);
    }

    private void calculateDiscount() {
        if (this.yearsServed >= maxTimeBasedThreshold) {
            this.discount = maxTimeBasedDiscount;
        } else if (this.yearsServed >= minTimeBasedThreshold) {
            this.discount = minTimeBasedDiscount;
        } else if (this.total >= maxTotalBasedThreshold) {
            this.discount = maxTotalBasedDiscount;
        } else if (this.total >= minTotalBasedThreshold) {
            this.discount = minTotalBasedDiscount;
        } else {
            this.discount = DEFAULT_DISCOUNT;
        }
    }

    ;

    public String getOrderDate() {
        return this.orderDate.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
    }

    public static void setDefaultDiscount(double defaultDiscount) {
        DEFAULT_DISCOUNT = defaultDiscount;
    }

    public static void setMaxTimeBasedDiscount(double maxTimeBasedDiscount) {
        Order.maxTimeBasedDiscount = maxTimeBasedDiscount;
    }

    public static void setMaxTimeBasedThreshold(int maxTimeBasedThreshold) {
        Order.maxTimeBasedThreshold = maxTimeBasedThreshold;
    }

    public static void setMaxTotalBasedDiscount(double maxTotalBasedDiscount) {
        Order.maxTotalBasedDiscount = maxTotalBasedDiscount;
    }

    public static void setMaxTotalBasedThreshold(double maxTotalBasedThreshold) {
        Order.maxTotalBasedThreshold = maxTotalBasedThreshold;
    }

    public static void setMinTimeBasedDiscount(double minTimeBasedDiscount) {
        Order.minTimeBasedDiscount = minTimeBasedDiscount;
    }

    public static void setMinTimeBasedThreshold(int minTimeBasedThreshold) {
        Order.minTimeBasedThreshold = minTimeBasedThreshold;
    }

    public static void setMinTotalBasedDiscount(double minTotalBasedDiscount) {
        Order.minTotalBasedDiscount = minTotalBasedDiscount;
    }

    public static void setMinTotalBasedThreshold(double minTotalBasedThreshold) {
        Order.minTotalBasedThreshold = minTotalBasedThreshold;
    }

    public void printOrder() {
        printDetails();
        printOrderItems();
        printOrderTotal();
    }

    protected void printOrderTotal() {
        System.out.println("Order Total: R" + OrderUtils.formatFloat(this.getTotal()));
    }

    ;

    private void printOrderItems() {
        System.out.println("Items:");
        for (OrderItem item : this.orderItems) {
            OrderUtils.printOrderItemSeparatorLine();
            item.print();
            OrderUtils.printOrderItemSeparatorLine();
        }
    }

    private void printDetails() {
        System.out.println("Order: " + this.orderNum);
        System.out.println("Order Type: " + this.getOrderType());
        System.out.println("Date: " + this.getOrderDate());
        this.calculateDiscount();
        System.out.println("Discount: %" + OrderUtils.formatFloat(this.discount));
    }

    public String getOrderType() {
        return "Order";
    }
}

