package com.psybergate.grad2021.core.oopart2.ce2a;

public class Exercise {
    private int i;
    private String s;

    public Exercise(int i, String s) {
        this.i = i;
        this.s = s;
    }

    public void print() {
        System.out.println("this.i = " + this.i + " this.s = " + this.s);
    }
}
