package com.psybergate.grad2021.core.oopart2.hw5a.hw5a1.v1;

public class InternationalOrder extends Order {
    private String country;

    public InternationalOrder(String orderNum, String country) {
        super(orderNum);
        this.country = country;
    }

    public String getOrderType() {
        return "International Order";
    }

    @Override
    public void printOrder() {
        super.printOrder();
        System.out.println("Country: " + country);
    }
}
