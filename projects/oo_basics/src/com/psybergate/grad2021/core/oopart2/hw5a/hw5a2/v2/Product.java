package com.psybergate.grad2021.core.oopart2.hw5a.hw5a2.v2;

public class Product {
    private String barcode;
    private String name;
    private double price;

    public Product(String barcode, String name, double price) {
        this.barcode = barcode;
        this.name = name;
        this.price = price;
    }

    public void printDetails() {
        System.out.println("Barcode: " + this.barcode);
        System.out.println("Item: " + this.name);
    }

    public double getPrice() {
        return this.price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (this.getClass() != o.getClass()) {
            return false;
        }

        Product product = (Product) o;
        return this.barcode.equals(product.barcode);
    }
}
