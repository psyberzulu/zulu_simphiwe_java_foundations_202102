package com.psybergate.grad2021.core.oopart2.hw5a.hw5a2.v1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;

public abstract class Order {
    protected static final double DEFAULT_DISCOUNT = 0;
    protected String orderNum;
    protected LocalDate date;
    protected List<OrderItem> orderItems;
    protected double discount;
    protected double total;


    public Order(String orderNum) {
        this(orderNum, DEFAULT_DISCOUNT);
    }

    public Order(String orderNum, double discount) {
        this.orderNum = orderNum;
        this.discount = discount;
        this.date = LocalDate.now();
        this.orderItems = new ArrayList<OrderItem>();
        this.total = 0;
    }

    public void addItem(Product product, int quantity) {
        OrderItem item = new OrderItem(product, quantity);
        this.addItem(item);
    }

    public void addItem(OrderItem item) {
        this.orderItems.add(item);
        this.total += item.getTotalPrice();
    }

    private void calculateTotal() {
        this.total = OrderUtils.getOrderTotal(orderItems) ;
    }

    public double getTotal() {
        return this.total  * (1.0 - this.discount/ 100.0);
    }

    protected abstract void calculateDiscount();

    public String getDate() {
        return this.date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM));
    }

    public void printOrder() {
        printDetails();
        printOrderItems();
        printOrderTotal();
    }

    protected void printOrderTotal() {
        System.out.println("Order Total: R" + OrderUtils.formatPrice(this.getTotal()));
    }

    ;

    private void printOrderItems() {
        System.out.println("Items:");
        for (OrderItem item : this.orderItems) {
            OrderUtils.printOrderItemSeparatorLine();
            item.print();
            OrderUtils.printOrderItemSeparatorLine();
        }
    }

    private void printDetails() {
        System.out.println("Order: " + this.orderNum);
        System.out.println("Order Type: " + this.getOrderType());
        System.out.println("Date: " + this.getDate());
    }

    public String getOrderType() {
        return "Order";
    }
}

