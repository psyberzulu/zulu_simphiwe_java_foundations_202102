package com.psybergate.grad2021.core.oopart2.hw5a.hw5a1.v2;

public class InternationalOrder extends Order {
    public static final String INTERNATIONAL_ORDER = "International Order";
    private String country;

    public InternationalOrder(String orderNum, Customer customer, String country) {
        super(orderNum, customer);
        this.country = country;
    }

    public String getOrderType() {
        return INTERNATIONAL_ORDER;
    }

    @Override
    public void printOrder() {
        super.printOrder();
        System.out.println("Country: " + country);
    }
}
