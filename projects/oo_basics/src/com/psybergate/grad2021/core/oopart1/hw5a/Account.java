package com.psybergate.grad2021.core.oopart1.hw5a;

public class Account {
    protected String accountNum;
    protected double balance;

    public Account() {
        this("00", 0);
//        this.accountNum = "0123456";
//        this.balance = 0;
    }

    public void withdraw(double amount) {
        this.balance -= amount;
    }

    public double getBalance() {
        return this.balance;
    }

    public Account(String accountNum) {
        this(accountNum, 0);
    }

    public Account(String accountNum, double balance) {
        this.accountNum = accountNum;
        this.balance = balance;
    }
}
