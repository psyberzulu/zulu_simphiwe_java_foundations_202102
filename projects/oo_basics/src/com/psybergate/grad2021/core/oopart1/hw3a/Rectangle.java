package com.psybergate.grad2021.core.oopart1.hw3a;

public class Rectangle {
    private int length1;
    private int length2;
    private static final int MAX_LENGTH1 = 200;
    private static final int MAX_LENGTH2 = 100;
    private static final int MAX_AREA = 15000;

    public static void main(String[] args) {
        Rectangle r1 = new Rectangle(150, 100);
        Rectangle r2 = new Rectangle(200, 100);
        Rectangle r3 = new Rectangle(350, 150);


    }

    public Rectangle(int length1, int length2) {
        if (this.validate(length1, length2)) {
            this.setLengths(length1, length2);
        }
    }

    private boolean validate(int length1, int length2) {
        if (length1 > length2) {
            return !(length1 > 200 || length2 > 100) && (length1 * length2) <= 15000;
        } else {
            return !(length2 > 200 || length1 > 100) && (length1 * length2) <= 15000;
        }
    }

    private void setLengths(int length1, int length2) {
        if (length1 > length2) {
            this.length1 = length1;
            this.length2 = length2;
        } else {
            this.length1 = length2;
            this.length2 = length1;
        }
    }

    public int calcArea() {
        return this.length1 * this.length2;
    }
}