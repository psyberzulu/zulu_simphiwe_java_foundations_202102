package com.psybergate.grad2021.core.oopart1.hw4a;

public class Company extends Customer{
    private String regNum;

    public Company() {
        this.regNum = "company";
    }

    public Company(String customerNum, String address, String email, String name, String regNum) {
        super(customerNum, address, email, name);
        this.regNum = regNum;
    }
}
