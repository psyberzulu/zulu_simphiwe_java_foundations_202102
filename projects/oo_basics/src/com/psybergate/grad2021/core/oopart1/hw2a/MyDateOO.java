package com.psybergate.grad2021.core.oopart1.hw2a;

public class MyDateOO {
    private String date;

    public MyDateOO() {
        this("01/01/2000");
    }

    public MyDateOO(String date) {
        this.date = date;
    }

    public void addDays(int days) {
        return;
    }

    public boolean isValid() {
        return true;
    }

    public int compareTo(MyDateOO date) {
        return this.date.compareTo(date.toString());
    }

    public String toString() {
        return this.date;
    }
}
