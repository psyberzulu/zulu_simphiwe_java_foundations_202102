package com.psybergate.grad2021.core.oopart1.hw2a;

import java.util.Date;

public class MyDatePP {
    public static boolean validateDate(String date) {
        return true;
    }

    public static String addDaysToDate(String date, int days) {
        return days + " days added to " + date;
    }

    public static int compareDates(String date1, String date2) {
        return date1.compareTo(date2);
    }
}
