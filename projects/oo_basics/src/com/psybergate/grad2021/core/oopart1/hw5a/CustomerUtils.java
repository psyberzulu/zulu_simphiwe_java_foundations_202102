package com.psybergate.grad2021.core.oopart1.hw5a;

import java.util.List;

public class CustomerUtils {


    public static void main(String[] args) {
        Customer customer = new Customer("01234", "Mr Gualo", "23 Bendo Hills, Trench Capital, 1943");
        Account account = new SavingsAccount();
        Account account1 = new CurrentAccount("012",100_000_000,0);
        customer.addAccount(account);
        customer.addAccount(account1);

        printBalance(customer);

    }


    public static void printBalance(Customer customer) {
        double total = 0;

        for (Account account: customer.getAccounts()) {
            total += account.getBalance();
        }
        System.out.println("Total balance: R" + total);
    }
}
