package com.psybergate.grad2021.core.oopart1.hw5a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private String customerNum;
    private String name;
    private String address;
    private List<Account> accounts;

    public Customer(String customerNum, String name, String address) {
        this.customerNum = customerNum;
        this.name = name;
        this.address = address;
        this.accounts = new ArrayList();
    }

    public void addAccount(Account account) {
        this.accounts.add(account);
    }


    public List<Account> getAccounts() {
        return accounts;
    }
}
