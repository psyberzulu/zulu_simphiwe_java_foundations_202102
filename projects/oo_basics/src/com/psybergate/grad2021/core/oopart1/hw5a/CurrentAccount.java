package com.psybergate.grad2021.core.oopart1.hw5a;

public class CurrentAccount extends Account {
    public static final int MAX_OVERDRAFT = 10000;
    private double overdraft;

    public CurrentAccount() {
        this("003", 0, MAX_OVERDRAFT);
    }

    public CurrentAccount(String accountNum, double balance, double overdraft) {
        super(accountNum, balance);
        this.overdraft = overdraft;
    }

    private boolean isOverDrawn() {
        return overdraft < MAX_OVERDRAFT;
    }
}
