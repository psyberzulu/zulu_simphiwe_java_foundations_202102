package com.psybergate.grad2021.core.oopart1.ce1a;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private String customerNum;
    private String name;
    private String address;
    private List accounts;

    public static void main(String[] args) {
        Customer customer = new Customer("01234", "Mr Gualo", "23 Bendo Hills, Trench Capital, 1943");
        //customer.createAccount();
        CustomerUtils.printBalance(customer.accounts);
    }

    public Customer(String customerNum, String name, String address) {
        this.customerNum = customerNum;
        this.name = name;
        this.address = address;
        this.accounts = new ArrayList();
    }

    public void addAccount() {
        Account account = new Account();
        this.accounts.add(account);
    }
}
