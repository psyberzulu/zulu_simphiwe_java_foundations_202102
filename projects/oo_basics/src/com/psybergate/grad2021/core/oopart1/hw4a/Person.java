package com.psybergate.grad2021.core.oopart1.hw4a;

import java.time.LocalDate;

public class Person extends Customer{
    private String surname;
    private String dob;
    private int age;
    private static final int MINIMUM_AGE = 15;

    public Person() {
        super();
        this.surname = "person";
        this.dob = "01/01/1970";
    }

    public Person(String customerNum, String address, String email, String name, String surname, String dob) {
        super(customerNum, address, email, name);
        int age = this.calculateAge(dob);
        this.surname = surname;
        this.dob = dob;
        if (!validateAge(age)) {
//            super();
            throw new RuntimeException("Customer must be over the age of " + MINIMUM_AGE);
        }
    }

    private static int calculateAge(String dob) {
        return LocalDate.now().getYear() - Integer.parseInt(dob.split("/")[2]);
    }

    private static boolean validateAge(int age) {
        return age >= MINIMUM_AGE;
    }
}
