package com.psybergate.grad2021.core.oopart1.hw4a;

public class Customer {
    private String customerNum;
    private String address;
    private String email;
    private String name;
    private double credit = 0;
    private double maxCredit = 10000;

    public Customer() {
        this("new_customer", "n/a", "customer@email.com", "customer");
    }

    public Customer(String customerNum, String address, String email, String name) {
        this.customerNum = customerNum;
        this.address = address;
        this.email = email;
        this.name = name;
    }

    public void makePurchase(double amount) {
        if (this.validatePurchase(amount)) {
            this.credit += amount;
        }
    }

    private boolean validatePurchase(double amount) {
        return this.credit <= (maxCredit - amount);
    }

    public void makePayment(double amount) {
        this.credit -= amount;
    }
}
