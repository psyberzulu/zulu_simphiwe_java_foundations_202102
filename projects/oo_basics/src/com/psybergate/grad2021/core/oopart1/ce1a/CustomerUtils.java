package com.psybergate.grad2021.core.oopart1.ce1a;

import java.util.List;

public class CustomerUtils {

    public static void printBalance(List<Account> accounts) {
        double total = 0;

        for (Account account:
             accounts) {
            total += account.getBalance();
        }
        System.out.println("Total balance: R" + total);
    }
}
