package com.psybergate.grad2021.core.oopart3.hw1a;

public class VariableContribution{
    private double contribution;
    private double minimumContributionAmount;
    private double maximumContributionAmount;

    public VariableContribution(double contribution,
                                double minimumContributionAmount, double maximumContributionAmount) {
        this.contribution = contribution;
        this.minimumContributionAmount = minimumContributionAmount;
        this.maximumContributionAmount = maximumContributionAmount;
    }

    public double getContributionAmount(double salary) {
        double amount = salary * this.contribution;
        if (contribution >= maximumContributionAmount) {
            return maximumContributionAmount;
        } else if (contribution >= minimumContributionAmount) {
            return amount;
        } else {
            return minimumContributionAmount;
        }

    }
}
