package com.psybergate.grad2021.core.oopart3.hw1a;

public class Contribution {
    protected double contribution;

    public Contribution(double contribution) {
        this.contribution = contribution;
    }

    public double getContribution() {
        return contribution;
    }
}
