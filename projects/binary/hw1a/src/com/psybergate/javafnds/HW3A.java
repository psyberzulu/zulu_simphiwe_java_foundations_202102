package com.psybergate.javafnds;

public class HW3A {
    public static void main(String[] args) {
        String out = "";
        for (int i = 0; i < 1000; i++) {
            out += String.format("%11c ",(char) 0x20 + i);
        }

        System.out.println(out);
    }
}
