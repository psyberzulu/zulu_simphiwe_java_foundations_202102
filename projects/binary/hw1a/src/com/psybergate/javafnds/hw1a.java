package com.psybergate.javafnds;

import java.io.File;
import java.io.FileInputStream;

public class hw1a {
    public static void main(String[] args) {
//        String file = ".\\out\\production\\ce1a\\com\\psybergate\\javafnds\\ASCIIWriter.class";
//        String file = "hw1a.iml";
        String file = ".idea\\.gitignore";
        readBits(file);
//        char c = (char) 0x3d5;
//        System.out.println(c);
    }

    public static void readBits(String filePath) {
        try {
            File file = new File(filePath);
            FileInputStream inStream = new FileInputStream(filePath);
            String content = "";

            for (int i = inStream.read(); i > -1; i = inStream.read()) {
                content += String.format("%11s", Integer.toHexString(i));
//                content += String.format("%c", (char) i);
            }

            System.out.println(content);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
