package com.psybergate.javafnds;

import java.io.*;

public class ASCIIWriter {
    public static void main(String[] args) {
        String s = "The cat sat on the hat";
        writeToFile(s);
//        int[] sHex = {0x54, 0x68, 0x65, 0x20, 0x63, 0x61, 0x74, 0x20, 0x73, 0x61, 0x74, 0x20, 0x6F, 0x6E, 0x20, 0x74, 0x68, 0x65, 0x68, 0x61, 0x74};
        printBits("test.txt");
    }

    public static void writeToFile(String text) {
        try {
            File f = new File("test.txt");
            PrintWriter p = new PrintWriter(f, "US-ASCII");
            p.write(text);
            p.close();

        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

    }

    public static void printBits(String filePath) {
        try {
            File file = new File(filePath);
            FileInputStream inStream = new FileInputStream(filePath);

            String out = "";
            int i = inStream.read();
            while (i != -1) {
//                out += i + " "; // Print byte
//                out += String.format("0x%02x ", i); // Format byte to hex
//                out += String.format("0x%12x", i); // Format byte to hex
//                out += String.format("%11s ", Integer.toBinaryString(i)); // Format byte to binary
                out += String.format("%11s ", Integer.toHexString(i)); // Format byte to binary
                i = inStream.read();
            }

            System.out.println(out);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
