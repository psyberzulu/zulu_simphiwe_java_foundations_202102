package com.psybergate.hw4a_2;

import com.psybergate.hw4a_3.*;


public class ClassB {

    public static void callMe() {
        ClassC c = new ClassC();
        System.out.println("Class B: calling Class C");
        c.callMe();
    }
}