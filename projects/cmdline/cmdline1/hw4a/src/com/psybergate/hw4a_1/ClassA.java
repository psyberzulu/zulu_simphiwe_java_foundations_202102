package com.psybergate.hw4a_1;

import com.psybergate.hw4a_2.*;


public class ClassA {
    public static void main(String[] args) {
        System.out.println("Class A: main");
        callMe(0);
    }

    public static void callMe(int terminate) {
        ClassB b = new ClassB();
        if (terminate == 1) {
            return;
        }
        System.out.println("Class A: calling Class B");
        b.callMe();
    }
}