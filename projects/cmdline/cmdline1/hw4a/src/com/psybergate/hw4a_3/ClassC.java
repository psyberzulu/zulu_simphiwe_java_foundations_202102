package com.psybergate.hw4a_3;

import com.psybergate.hw4a_1.*;


public class ClassC {

    public static void callMe() {
        ClassA a = new ClassA();
        System.out.println("Class C: calling Class A");
        a.callMe(1);
    }
}