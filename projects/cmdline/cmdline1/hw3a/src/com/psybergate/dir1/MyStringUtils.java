package com.psybergate.dir1;

// import com.psybergate.dir2;

public class MyStringUtils {
    public static String reverse(String str) {
        // return "It works";
        String out = "";
        String[] arr = str.split("");

        for (int i = arr.length - 1; i > -1; i--) {
            out += arr[i];
        }

        return out;
    }
}