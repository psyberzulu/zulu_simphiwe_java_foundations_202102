package com.psybergate.dir2;

import com.psybergate.dir1.MyStringUtils;

public class StringReverse {
    public static void main(String[] args) {
        MyStringUtils m = new MyStringUtils();
        String name = m.reverse("Truh me!");

        System.out.println(name);
    }
}