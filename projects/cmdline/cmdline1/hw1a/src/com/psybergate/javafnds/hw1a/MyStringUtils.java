package com.psybergate.javafnds;

public class MyStringUtils {

    public static String reverse(String str) {
        String out = "";
        String arr[] = str.split("");

        for(int i = arr.length - 1; i > -1; i--) {
            out += arr[i];
        }

        return out;
    }
}