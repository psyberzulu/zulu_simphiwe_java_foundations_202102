package com.psybergate.javafnds;

public class StringReverse {
    public static void main(String[] args) {
        reverse();
    }

    public static void reverse() {
        MyStringUtils m = new MyStringUtils();
        String result = m.reverse("Simphiwe");
        System.out.println(result);
    }
}