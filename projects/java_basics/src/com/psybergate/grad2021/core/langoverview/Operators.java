package com.psybergate.grad2021.core.langoverview;

public class Operators {
    public static void main(String[] args) {
//        % operator
        int mod = 16 % 3;
        System.out.println("% operator:");
        System.out.println("16 % 3 = " + mod);

//        Pre-increment operator
        int i = 0;
        System.out.println("Pre-increment (i = " + i + "):");
        System.out.println("++i = " + ++i + "; i = " + i);

//        Post-increment operator
        i = 0;
        System.out.println("Post-increment (i = " + i + "):");
        System.out.println("i++ = " + i++ + "; i = " + i);

//        == operator
        boolean t = new Object() == new Object();
        System.out.println("== operator:");
        System.out.println("(new Object() == new Object) = " + t);
        System.out.println("(i == i) = " + (i == i));

//        && and & operators
        System.out.println("&& and & operators:");
        System.out.println("-2 & -4: " + (-2 & -4));
//        Next line won't be compiled since && only takes boolean operands
//        System.out.println("-2 && -4:" + (-2 && -4));
        System.out.println("false & true: " + (false & true));
        System.out.println("false && true: " + (false && true));

        //        && and & operators
        System.out.println("|| and | operators:");
        System.out.println("-2 | -4: " + (-2 | -4));
//        Next line won't be compiled since && only takes boolean operands
//        System.out.println("-2 && -4:" + (-2 && -4));
        System.out.println("false | true: " + (false | true));
        System.out.println("false || true: " + (false || true));


    }
}
