package com.psybergate.grad2021.core.langoverview;

import java.util.Scanner;

/**
 * This class goes through the static modifier.
 * Code is based on questions from hw2a
 */


public class StaticModifier {
    // Static variable - class variable
    private static int staticNum;

    // Static object reference - class object reference
    private static Object object;

    //    Instance variable
    private int[] instanceArr;

    //    Static initializer
    static {
//        Only static variables can be initialized in this block
        System.out.println("Location: static initializer block");
        staticNum = 5;
        object = new Scanner(System.in);

//        Throwing a runtime exception is not allowed by the compiler
//        unless it's in a try-catch block
//        throw new RuntimeException();
    }

    //    Instance initializer
    {
//        Instance variables are initialized here
        System.out.println("Location: instance initializer block");
        instanceArr = new int[20];
        for (int i : instanceArr) {
            i = 0;
        }
    }

    public static void main(String[] args) {
        StaticModifier sm = new StaticModifier();
        StaticModifier.staticMethod();
    }

    //    A constructor cannot be declared as static
    public StaticModifier() {
        System.out.println("Location: default constructor");
        staticNum = 7;
    }

    //    Static method
    public static void staticMethod() {
        System.out.println("Static num = " + staticNum);
    }
}
