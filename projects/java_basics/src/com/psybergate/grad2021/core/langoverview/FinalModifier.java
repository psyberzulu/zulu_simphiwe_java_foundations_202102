package com.psybergate.grad2021.core.langoverview;

// A final class cannot be extended by another class
// (i.e. can't be inherited by another class).
public final class FinalModifier {
    private static final int PRIVATE_STATIC_INT;

    private final int privInt;

    static {
        PRIVATE_STATIC_INT = 0;
    }

    // final modifier cannot be applied to a constructor
    public FinalModifier() {
//        Next statement is invalid since PRIVATE_STATIC_INT
//        has already been assigned a value
//        PRIVATE_STATIC_INT = 2;
        privInt = 0;
    }

    //    final method cannot be overridden by subclasses
    public final void finalMethod() {
        System.out.println("Location: finalMethod()");

//        The next statement is invalid since privInt was
//        assigned a value in the constructor
//        privInt = 1;
    }

    public void method(final int finalParameter) {
//        Assigning values to final parameters is not allowed therefore
//        the next statement is invalid
//        finalParameter = 3;
    }

//    private methods are implicitly final
//    private final void privateFinalMethod() {
//
//    }
}
