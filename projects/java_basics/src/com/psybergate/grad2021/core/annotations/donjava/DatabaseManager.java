package com.psybergate.grad2021.core.annotations.donjava;

import com.sun.xml.internal.bind.v2.TODO;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DatabaseManager {

  public static void main(String[] args) {
    DatabaseManager db_404 =
        new DatabaseManager(DATABASE_URL, "db_404", PASSWORD, USERNAME);
    try {
      db_404.connect();
      db_404.dropTable(Customer.class);
      db_404.createTable(Customer.class);
      db_404.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static final String DATABASE_URL = "jdbc:postgresql://localhost" +
      ":5432/";

  private static final String USERNAME = "postgres";

  private static final String PASSWORD = "admin";

  private Connection connection;

  private String databaseUrl;

  private String databaseName;

  private String password;

  private String username;

  public DatabaseManager(String databaseUrl, String databaseName, String password, String username) {
    this.databaseUrl = databaseUrl;
    this.databaseName = databaseName;
    this.password = password;
    this.username = username;
  }

  public DatabaseManager(String databaseUrl, String password, String username) {
    this.connection = null;
    this.databaseUrl = databaseUrl;
    this.password = password;
    this.username = username;
  }

  public void connect() throws ClassNotFoundException, SQLException {
    try {
      Class.forName("org.postgresql.Driver");
      String url = databaseUrl + databaseName;
      connection = DriverManager.getConnection(url, username,
          password);
      System.out.println("Database successfully opened");
    } catch (SQLException sqlException) {
      if (sqlException.getErrorCode() ==  0) {
        this.createDatabase();
      } else {
        throw sqlException;
      }
    }
  }

  private void createDatabase() throws SQLException, ClassNotFoundException {
      Class.forName("org.postgresql.Driver");
      this.connection = DriverManager.getConnection(this.databaseUrl,
          this.username, this.password);

      if (this.connection != null) {
        Statement statement = this.connection.createStatement();
        String query = "create database " + this.databaseName + ";";
        statement.executeUpdate(query);
        statement.close();
      }
  }

  public <T> void createTable(Class<T> clazz) throws SQLException {
    DatabaseTable table = DatabaseTable.fromClass(clazz);
    Statement statement = this.connection.createStatement();
    String query = table.asCreateQuery();
    statement.executeUpdate(query);
    statement.close();
    System.out.println("Table " +  table.getName() + " created from " + clazz.getName());
  }

  public <T> void dropTable(Class<T> clazz) throws SQLException {
    DatabaseTable table = DatabaseTable.fromClass(clazz);
    Statement statement = this.connection.createStatement();
    String query = table.asDropQuery();
    statement.executeUpdate(query);
    statement.close();
    System.out.println("Table " + table.getName() + " dropped from database");
  }

  public <T> Collection<T> selectAllFromTable(Class<T> clazz) throws SQLException {
    DatabaseTable table = DatabaseTable.fromClass(clazz);
    Statement statement = this.connection.createStatement();
    String query = table.asSelectAllQuery();
    Collection<T> collection = new ArrayList<>();

//    TODO: Finish it!
    return collection;
  }

  private static String generateSelectQuery(String table,
                                            List<String> fields) {
    StringBuilder query = new StringBuilder("select ");

    if (fields != null && !fields.isEmpty()) {
      int i = fields.size() - 1;
      for (String field : fields) {
        query.append(field);
        if (i > 0) {
          query.append(", ");
        }
        i--;
      }
    } else {
      query.append("*");
    }

    query.append(" from " + table);
    return query.toString();
  }

  public void close() throws SQLException {
    this.connection.close();
  }

}
