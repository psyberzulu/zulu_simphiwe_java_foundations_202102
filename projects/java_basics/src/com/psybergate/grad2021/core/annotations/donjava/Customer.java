package com.psybergate.grad2021.core.annotations.donjava;

@DomainClass(name = "customer")
public class Customer {
  @DomainProperty(primaryKey = true, name = "customer_num")
  private String customerNum;

  @DomainProperty(name = "first_name")
  private String name;

  @DomainProperty(name = "surname")
  private String surname;

  @DomainProperty(name = "date_of_birth")
  private Integer dateOfBirth;

  @DomainTransient
  private int age;
}
