package com.psybergate.grad2021.core.annotations.donjava;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DatabaseTableField {
  private String name;

  private String fieldType;

  private List<FieldConstraint> constraints;

  public DatabaseTableField(String name, String fieldType, List<FieldConstraint> constraints) {
    this.name = name;
    this.fieldType = fieldType;
    this.constraints = constraints;
  }

  public List<FieldConstraint> getConstraints() {
    return this.constraints;
  }

  public String getName() {
    return this.name;
  }

  public String getFieldType() {
    return fieldType;
  }

  public static DatabaseTableField fromInstanceField(Field field) {
    Annotation[] annotations = field.getDeclaredAnnotations();

    for (Annotation annotation : annotations) {
      if (annotation instanceof DomainProperty) {
        List<FieldConstraint> constraints = new ArrayList<>();
        DomainProperty domainProperty = (DomainProperty) annotation;
        String name = domainProperty.name();

        if (domainProperty.primaryKey()) {
          constraints.add(FieldConstraint.PRIMARY_KEY);
        }

        if (domainProperty.foreignKey()) {
          constraints.add(FieldConstraint.FOREIGN_KEY);
        }

        if (!domainProperty.nullable()) {
          constraints.add(FieldConstraint.NOT_NULL);
        }

        if (domainProperty.unique()) {
          constraints.add(FieldConstraint.UNIQUE);
        }

        String type = getFieldTypeFromField(field);

        return new DatabaseTableField(name, type, constraints);
      }
    }

    throw new IllegalArgumentException();
  }

  private static String getFieldTypeFromField(Field field) {
    if (field.getType().equals(String.class)) {
      return "varchar(255)";
    }

    if (field.getType().equals(int.class) || field.getType().equals(Integer.class)) {
      return "integer";
    }

    if (field.getType().equals(double.class)) {
      return "float";
    }

    if (field.getType().equals(boolean.class)) {
      return "boolean";
    }

    return "";
  }

  public String asQuery() {
    StringBuilder query = new StringBuilder(this.name + " " + this.fieldType);

    for (FieldConstraint constraint : this.constraints) {
      if (constraint == FieldConstraint.PRIMARY_KEY || constraint == FieldConstraint.FOREIGN_KEY) {
        continue;
      }
      query.append(" " + constraint.getValue());
    }

    return query.toString();
  }

  public boolean isPrimaryKey() {
    return this.constraints.contains(FieldConstraint.PRIMARY_KEY);
  }

  public boolean isForeignKey() {
    return this.constraints.contains(FieldConstraint.FOREIGN_KEY);
  }
}
