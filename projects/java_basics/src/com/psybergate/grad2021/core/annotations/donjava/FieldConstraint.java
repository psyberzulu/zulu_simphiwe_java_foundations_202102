package com.psybergate.grad2021.core.annotations.donjava;

public enum FieldConstraint {
  NOT_NULL("not null"), UNIQUE("unique"), PRIMARY_KEY("primary key"),
  FOREIGN_KEY("foreign key");

  private final String value;

  FieldConstraint(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }
}
