package com.psybergate.grad2021.core.annotations.donjava;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class DatabaseTable {
  private String name;
  private List<DatabaseTableField> tableFields;

  private DatabaseTable(String name, List<DatabaseTableField> tableFields) {
    this.name = name;
    this.tableFields = tableFields;
  }

  public static <T> DatabaseTable fromClass(Class<T> clazz) {
    if (!clazz.isAnnotationPresent(DomainClass.class)) {
      throw new IllegalArgumentException();
    }

    List<DatabaseTableField> tableFields = new ArrayList<>();
    DomainClass annotation = clazz.getAnnotation(DomainClass.class);
    Field[] clazzFields = clazz.getDeclaredFields();

    for (Field field : clazzFields) {
      if (field.isAnnotationPresent(DomainProperty.class)) {
        tableFields.add(DatabaseTableField.fromInstanceField(field));
      }
    }

    return new DatabaseTable(annotation.name(), tableFields);
  }

  public String asCreateQuery() {
    StringBuilder query = new StringBuilder("create table " + this.name + " (");
    StringBuilder foreignKey = new StringBuilder();
    StringBuilder primaryKey =
        new StringBuilder();

    int i = this.tableFields.size() - 1;
    for (DatabaseTableField field : this.tableFields) {
      query.append(" " + field.asQuery());

      if (field.isPrimaryKey()) {
        primaryKey.append(FieldConstraint.PRIMARY_KEY.getValue() + " (" + field.getName() + ")");
      }

      if (field.isForeignKey()) {
        foreignKey.append(FieldConstraint.FOREIGN_KEY.getValue() + " (" + field.getName() + ")");
      }

      if (i > 0) {
        query.append(",");
      }
      i--;
    }

    if (!primaryKey.toString().isEmpty()) {
      query.append(", " + primaryKey);
    }

    if (!foreignKey.toString().isEmpty()) {
      query.append(", " + foreignKey);
    }

    query.append(")");

    return query.toString();
  }

  public String asDropQuery() {
    return "drop table " + this.name;
  }

  public String asSelectAllQuery() {
    return "select * from " + this.name;
  }

  public String getName() {
    return name;
  }
}
