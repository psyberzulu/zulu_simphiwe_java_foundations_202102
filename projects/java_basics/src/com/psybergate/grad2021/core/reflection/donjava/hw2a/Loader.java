package com.psybergate.grad2021.core.reflection.donjava.hw2a;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class Loader {
  public static final Properties PROPERTIES;

  static {
    PROPERTIES = new Properties();
    InputStream inputStream =
        Loader.class.getClassLoader().getResourceAsStream( "com\\psybergate" +
            "\\grad2021\\core\\reflection\\donjava\\hw2a\\app" +
            ".properties");
    try {
      PROPERTIES.load(inputStream);
    } catch (IOException e) {
      System.err.println(e.getMessage());
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    String listImplementation = PROPERTIES.getProperty("list");
    try {
      Class<List> clazz = (Class<List>) Class.forName(listImplementation);
      List<String> list = populateList(clazz.newInstance(), 12);
      printList(list);
    } catch (Exception e) {
      System.err.println(e.getMessage());
    }
  }

  public static List<String> populateList(List<String> strings, int size) {
    for (int i = 0; i < size; i++) {
      strings.add("String" + i);
    }
    return strings;
  }

  public static void printList(List<String> strings) {
    for (String string : strings) {
      System.out.println(string);
    }
  }
}
