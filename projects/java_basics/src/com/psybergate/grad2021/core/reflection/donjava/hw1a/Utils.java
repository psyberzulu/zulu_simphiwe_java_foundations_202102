package com.psybergate.grad2021.core.reflection.donjava.hw1a;

import com.psybergate.grad2021.core.reflection.donjava.ce1a.ReflectionUtils;
import com.psybergate.grad2021.core.reflection.donjava.ce1a.Student;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Utils {
  public static void main(String[] args) {
    ReflectionUtils.printClassName(Student.class);
    ReflectionUtils.printSuperClassName(Student.class);
    ReflectionUtils.printMemberModifiers(Student.class);
    printMethodsReturnType(Student.class);
  }

  public static <T> void printMethodsReturnType(Class<T> clazz) {
    Method[] methods = clazz.getDeclaredMethods();
    System.out.println(clazz.getSimpleName() + " methods:");
    for (int i = 0; i < methods.length; i++) {
      printListElement(i,
          methods[i].getName() + ": " + methods[i].getReturnType().getName(), 1
          );
    }
  }

  private static void printListElement(int index, String element,
                                       int numberOfTabSpaces) {
    StringBuilder tabs = new StringBuilder();
    for (int i = 0; i < numberOfTabSpaces; i++) {
      tabs.append("\t");
    }
    System.out.println(tabs.toString() + index + ") " + element);
  }

}
