package com.psybergate.grad2021.core.reflection.donjava.ce2a;

import com.psybergate.grad2021.core.reflection.donjava.ce1a.Student;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

public class ReflectionUtils {
  public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InstantiationException, InvocationTargetException {
    Class<Student> studentClass = Student.class;

    Student student = studentClass.newInstance();

    Student student1 =
        studentClass.getConstructor(String.class, String.class, String.class,
            Integer.class).newInstance("S201", "Lil", "Student", 0);

    System.out.println("student.getName() = " + student.getName());
    System.out.println("student1.getName() = " + student1.getName());
  }

}
