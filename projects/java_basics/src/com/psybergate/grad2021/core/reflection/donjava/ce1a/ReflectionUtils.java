package com.psybergate.grad2021.core.reflection.donjava.ce1a;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

public class ReflectionUtils {
  public static void main(String[] args) throws NoSuchMethodException {
    Class<Student> studentClass = Student.class;
    printClassName(studentClass);
    printPackageName(studentClass);
    printAnnotations(studentClass);
    printConstructorParameterList(studentClass.getConstructor());
    printFields(studentClass);
    printInmplementedInterfaces(studentClass);
    printMethods(studentClass);
    printMemberModifiers(studentClass);
  }

  public static <T> void printClassName(Class<T> clazz) {
    System.out.println("Class name: " + clazz.getName());
  }

  public static <T> void printPackageName(Class<T> clazz) {
    System.out.println("Package name: " + clazz.getPackage().getName());
  }

  public static <T> void printAnnotations(Class<T> clazz) {
    Annotation[] annotations = clazz.getDeclaredAnnotations();
    System.out.println(clazz.getName() + " declared annotations:");
    for (int i = 0; i < annotations.length; i++) {
      printListElement(i, annotations[i].getClass().getName(), 1);
    }
  }

  public static <T> void printInmplementedInterfaces(Class<T> clazz) {
    Class<?>[] interfaces = clazz.getInterfaces();
    System.out.println(clazz.getName() + " implements:");
    for (int i = 0; i < interfaces.length; i++) {
      printListElement(i, interfaces[i].getName(), 1);
    }
  }

  public static <T> void printPublicFields(Class<T> clazz) {
    Field[] fields = clazz.getFields();
    System.out.println(clazz.getName() + " public fields:");
    for (int i = 0; i < fields.length; i++) {
      printListElement(i, fields[i].getName(), 1);
    }
  }

  public static <T> void printFields(Class<T> clazz) {
    Field[] fields = clazz.getDeclaredFields();
    System.out.println(clazz.getName() + " declared fields:");
    for (int i = 0; i < fields.length; i++) {
      printListElement(i, fields[i].getName(), 1);
    }
  }

  public static <T> void printConstructorParameterList(Constructor<T> constructor) {
    Parameter[] parameters = constructor.getParameters();
    System.out.println(constructor.getName() + " parameter list:");
    for (int i = 0; i < parameters.length; i++) {
      printListElement(i, parameters[i].getName(), 1);
    }

  }

  public static <T> void printMethods(Class<T> clazz) {
    Method[] methods = clazz.getDeclaredMethods();
    System.out.println(clazz.getName() + " declared methods:");
    for (int i = 0; i < methods.length; i++) {
      printListElement(i, methods[i].getName(), 1);
    }
  }

  public static <T> void printSuperClassName(Class<T> clazz) {
    Class<? super T> superclass = clazz.getSuperclass();
    System.out.println(clazz.getSimpleName() + " extends: " + superclass.getName());
  }

  public static <T> void printMemberModifiers(Class<T> clazz) {
    System.out.println(clazz.getSimpleName() + " members:");

    int counter = 0;
    printListElement(counter, "Constructors:", 1);
    Constructor<?>[] constructors = clazz.getConstructors();
    for (int i = 0; i < constructors.length; i++) {
      printListElement(i,
          constructors[i].getName() + ": " + Modifier.toString(constructors[i].getModifiers()), 2);
    }

    counter++;
    printListElement(counter, "Fields:", 1);
    Field[] fields = clazz.getDeclaredFields();
    for (int i = 0; i < fields.length; i++) {
      printListElement(i,
          fields[i].getName() + ": " + Modifier.toString(fields[i].getModifiers()), 2);
    }

    counter++;
    printListElement(counter, "Methods:", 1);
    Method[] methods = clazz.getDeclaredMethods();
    for (int i = 0; i < methods.length; i++) {
      printListElement(i,
          methods[i].getName() + ": " + Modifier.toString(methods[i].getModifiers())
          , 2);
    }
  }

  private static void printListElement(int index, String element,
                                       int numberOfTabSpaces) {
    StringBuilder tabs = new StringBuilder();
    for (int i = 0; i < numberOfTabSpaces; i++) {
      tabs.append("\t");
    }
    System.out.println(tabs.toString() + index + ") " + element);
  }
}
