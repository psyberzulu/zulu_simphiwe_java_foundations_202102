package com.psybergate.grad2021.core.reflection.donjava.ce1a;

import com.psybergate.grad2021.core.annotations.donjava.DomainClass;

import java.io.Serializable;

@DomainClass(name = "student")
public class Student implements Serializable {
  private String studentNum;

  private String name;

  private String surname;

  private Integer dateOfBirth;

  public String faculty;

  public Student(String studentNum, String name, String surname, Integer dateOfBirth, String faculty) {
    this.studentNum = studentNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.faculty = faculty;
  }

  public Student(String studentNum, String name, String surname, Integer dateOfBirth) {
    this(studentNum, name, surname, dateOfBirth, "");
  }

  public Student() {
    this("", "", "", 0, "");
  }

  public String getStudentNum() {
    return studentNum;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public Integer getDateOfBirth() {
    return dateOfBirth;
  }

  public String getFaculty() {
    return faculty;
  }

  public int getAge() {
    return 20;
  }
}
