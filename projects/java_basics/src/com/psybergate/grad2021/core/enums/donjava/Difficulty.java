package com.psybergate.grad2021.core.enums.donjava;

public class Difficulty {
  public static final Difficulty BEGINNER;

  public static final Difficulty AMATEUR;

  public static final Difficulty SEMI_PRO;

  public static final Difficulty PROFESSIONAL;

  public static final Difficulty WORLD_CLASS;

  public static final Difficulty LEGENDARY;

  public static final Difficulty ULTIMATE;

  static {
    BEGINNER = new Difficulty("Beginner", 1, 10, 10, 20, 15);
    AMATEUR = new Difficulty("Amateur", 2, 20, 20, 40, 30);
    SEMI_PRO = new Difficulty("Semi-pro", 3, 40, 40, 50, 50);
    PROFESSIONAL = new Difficulty("Professional", 4, 50, 50, 70, 60);
    WORLD_CLASS = new Difficulty("World Class", 5, 75, 75, 80, 80);
    LEGENDARY = new Difficulty("Legendary", 6, 90, 90, 90, 90);
    ULTIMATE = new Difficulty("Ultimate", 7, 100, 90, 100, 100);
  }

  private final String name;

  private final int rank;

  private final int attackingIntelligence;

  private final int defendingIntelligence;

  private final int reactionSpeed;

  private final int markingAndSpace;

  private Difficulty(String name, int rank, int attackingIntelligence, int defendingIntelligence, int reactionSpeed, int markingAndSpace) {
    this.name = name;
    this.rank = rank;
    this.attackingIntelligence = attackingIntelligence;
    this.defendingIntelligence = defendingIntelligence;
    this.reactionSpeed = reactionSpeed;
    this.markingAndSpace = markingAndSpace;
  }

  public int getAttackingIntelligence() {
    return attackingIntelligence;
  }

  public int getDefendingIntelligence() {
    return defendingIntelligence;
  }

  public int getReactionSpeed() {
    return reactionSpeed;
  }

  public int getMarkingAndSpace() {
    return markingAndSpace;
  }

  public int getRank() {
    return rank;
  }

  public String getName() {
    return name;
  }
}
