package com.psybergate.grad2021.core.enums.donjava;

public enum DifficultyLevel {
  BEGINNER(10, 10, 20, 15),
  AMATEUR(20, 20, 40, 30),
  SEMI_PRO(40, 40, 50, 50),
  PROFESSIONAL(50, 50, 70, 60),
  WORLD_CLASS(75, 75, 80, 80),
  LEGENDARY(90, 90, 90, 90),
  ULTIMATE(100, 90, 100, 100);

  private final int attackingIntelligence;

  private final int defendingIntelligence;

  private final int reactionSpeed;

  private final int markingAndSpace;

  DifficultyLevel(int attackingIntelligence, int defendingIntelligence, int reactionSpeed, int markingAndSpace) {
    this.attackingIntelligence = attackingIntelligence;
    this.defendingIntelligence = defendingIntelligence;
    this.reactionSpeed = reactionSpeed;
    this.markingAndSpace = markingAndSpace;
  }

  public int getAttackingIntelligence() {
    return attackingIntelligence;
  }

  public int getDefendingIntelligence() {
    return defendingIntelligence;
  }

  public int getReactionSpeed() {
    return reactionSpeed;
  }

  public int getMarkingAndSpace() {
    return markingAndSpace;
  }
}
