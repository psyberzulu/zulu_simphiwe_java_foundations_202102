package com.psybergate.grad2021.core.concurrency.donjava.ce1a;

public class Comma{
  private int level;

  public Comma() {
    this.level = 0;
  }

  public synchronized void runItUp(String threadName) {
    this.level++;
    System.out.println(threadName + ": ran it up to " + this.level);
  }

  public int getLevel() {
    return level;
  }
}
