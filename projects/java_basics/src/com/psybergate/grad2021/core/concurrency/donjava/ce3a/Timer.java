package com.psybergate.grad2021.core.concurrency.donjava.ce3a;

import java.time.Duration;
import java.time.Instant;

public class Timer {
  private Instant start;
  private Instant end;

  public Timer() {
  }

  public void start() {
    this.start = Instant.now();
  }

  public void stop() {
    this.end = Instant.now();
  }

  public int getDuration() {
    return Duration.between(this.start, this.end).getNano();
  }
}
