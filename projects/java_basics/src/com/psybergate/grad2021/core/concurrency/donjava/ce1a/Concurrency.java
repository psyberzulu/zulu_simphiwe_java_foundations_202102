package com.psybergate.grad2021.core.concurrency.donjava.ce1a;

import java.util.ArrayList;
import java.util.List;

public class Concurrency {
  public static void main(String[] args) {
//    startRunners(10);
    startThrowers(2);

//    Comma comma = null;
//    comma = runUpTheComma(3, new Comma());
//    System.out.println("comma.getLevel() = " + comma.getLevel());
  }

  public static void joinAll(List<Thread> threads) {
    for (Thread thread : threads) {
      try {
        thread.join();
      } catch (InterruptedException e) {
        System.err.println(thread.getName() + " couldn't join us...");
        e.printStackTrace();
      }
    }
  }

  /**
   * Starts threads using a Runnable
   * @param total - number of threads
   */
  public static void startRunners(int total) {
    System.out.println("Main: starting " + total + " runners...");
    for (int i = 0; i < total; i++) {
      Runnable runnable = new Runner(i);
      Thread thread = new Thread(runnable);
      thread.start();
    }
  }

  /**
   * Starts threads using an annonymous class that extends Thread
   * @param total - number of threads
   */
  public static void startThrowers(int total) {
    System.out.println("Main: starting " + total + " throwers...");

    for (int i = 0; i < total; i++) {
      Thread thread = new Thread() {
        @Override
        public void run() {
          try {
            throwExceptionFromDeep(2);
          } catch (Exception e) {
            System.out.println(this.getName() + ": " + e.getMessage());
            e.printStackTrace();
          }
        }
      };
      thread.start();
    }
  }

  public static Comma runUpTheComma(int totalRunners, Comma comma) {
    List<Thread> threads = new ArrayList<>();
    for (int i = 0; i < totalRunners; i++) {
      Thread thread = new CommaRunner(comma);
      thread.start();
      threads.add(thread);
    }
    joinAll(threads);
    return comma;
  }


  public static void throwExceptionFromDeep(int level) throws Exception {
    throwExceptionFromDeep(level, level - 1);
  }

  private static void throwExceptionFromDeep(int level, int counter) throws Exception {
    if (counter < 2) {
      throw new Exception("Thrown from level " + level);
    }
    throwExceptionFromDeep(level, counter - 1);
  }
}
