package com.psybergate.grad2021.core.concurrency.donjava.hw1a.nonpolling;


public class MessageWriter {
  private final MessageCache cache;

  public MessageWriter() {
    this.cache = MessageCache.getInstance();
  }

  public boolean write(Message message) {
    return this.cache.add(message);
  }
}
