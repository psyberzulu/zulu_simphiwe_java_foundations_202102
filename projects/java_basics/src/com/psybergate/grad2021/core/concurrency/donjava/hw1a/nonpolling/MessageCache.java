package com.psybergate.grad2021.core.concurrency.donjava.hw1a.nonpolling;

import java.util.ArrayList;
import java.util.List;

public class MessageCache {

  private static final MessageCache CACHE;

  private final List<Message> messages;

  static {
    CACHE = new MessageCache();
  }

  public static MessageCache getInstance() {
    return CACHE;
  }

  private MessageCache() {
    this.messages = new ArrayList<>();
  }

  public boolean add(Message message) {
    synchronized (this.messages) {

      boolean add = this.messages.add(message);
      this.messages.notify();
      return add;
    }
  }

  public boolean isEmpty() {
    synchronized (this.messages) {
      return this.messages.isEmpty();
    }
  }

  public Message read() {
    synchronized (this.messages) {
      if (this.isEmpty()) {
        try {
          this.messages.wait();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }

      return this.messages.remove(0);
    }
  }
}
