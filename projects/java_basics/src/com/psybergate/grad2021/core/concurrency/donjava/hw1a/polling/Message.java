package com.psybergate.grad2021.core.concurrency.donjava.hw1a.polling;

public class Message {
  private final String message;

  public Message(String message) {
    this.message = message;
  }

  public String getMessage() {
    return message;
  }
}
