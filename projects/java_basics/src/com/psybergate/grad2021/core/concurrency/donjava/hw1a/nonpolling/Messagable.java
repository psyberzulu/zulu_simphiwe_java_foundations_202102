package com.psybergate.grad2021.core.concurrency.donjava.hw1a.nonpolling;

import com.psybergate.grad2021.core.concurrency.codeandchill.hw1a.nonpolling.Messenger;

import java.util.ArrayList;
import java.util.List;

/**
 * A runnable that can send and receive messages through its messenger. Hence,
 * you can run and send messages to the team in code.
 */
public class Messagable implements Runnable {

  public static final int POOL_SIZE = 8;

  public static void main(String[] args) {

    Messagable m1 = new Messagable();

    List<Messagable> messagables = createPool();

    List<Thread> threads = createThreadPool(messagables);

    startThreads(threads);

  }

  private static void startThreads(List<Thread> threads) {
    for (int i = 0; i < threads.size(); i++) {
      threads.get(i).start();
    }
  }

  private static List<Thread> createThreadPool(List<Messagable> messagables) {

    List<Thread> threads = new ArrayList<>();

    for (int i = 0; i < messagables.size(); i++) {
      threads.add(new Thread(messagables.get(i)));
    }

    return threads;
  }

  private static List<Messagable> createPool() {
    List<Messagable> pool = new ArrayList<>();

    for (int i = 0; i < POOL_SIZE; i++) {
      pool.add(new Messagable());
    }

    return pool;
  }

  // TODO: Explore volatile
  private volatile Messenger messenger;

  private boolean running;

  public Messagable() {
    this.messenger = new Messenger();
    this.running = true;
  }

  private void stop() {
    this.running = false;
  }

  private void start() {
    this.running = true;
  }

  private boolean sendMessage(String message) {
    return this.messenger.send(message);
  }

  private Message receiveMessage() {
    return this.messenger.read();
  }

  @Override
  public void run() {

    int counter = 0;

    while (this.running) {
      System.out.println(Thread.currentThread().getName() + ": I'm running...");

      try {
        Thread.sleep((int) (420 * Math.random()));
        boolean sent =
            this.sendMessage(Thread.currentThread().getName() + "I'm saying " +
                "something...");
        if (sent) {
          Message message = this.receiveMessage();
          System.out.println(Thread.currentThread().getName() + ": I heard " +
              "something..." );
        }

        if (counter > 10) {
          stop();
        }
      } catch (InterruptedException e) {
        e.printStackTrace();
      }

      counter++;
    }
  }
}
