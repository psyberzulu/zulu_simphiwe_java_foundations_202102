package com.psybergate.grad2021.core.concurrency.donjava.ce3a;

import java.util.List;

public class AdderRunnable implements Runnable{

  private Adder adder;
  private long sum;

  public AdderRunnable(Adder adder) {
    this.adder = adder;
  }

  @Override
  public void run() {
    this.sum = this.adder.sum();
  }

  public static long sumAllResults(List<AdderRunnable> adderRunnables) {
    long result = 0;

    for (AdderRunnable adderRunnable : adderRunnables) {
      result += adderRunnable.sum;
    }

    return result;
  }
}
