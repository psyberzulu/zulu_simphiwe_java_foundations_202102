package com.psybergate.grad2021.core.concurrency.donjava.ce3a;

import java.util.ArrayList;
import java.util.List;

public class ConcurrentAdder {
  public static void main(String[] args) {

    Adder adder = new Adder(1, 1_000_000);
    long sum = adder.sum();
    System.out.println("sum = " + sum);

    ConcurrentAdder concurrentAdder = new ConcurrentAdder(3);
    long sum1 = concurrentAdder.sum(1, 1_000_000);
    System.out.println("sum1 = " + sum1);

  }

  private int numOfThreads;

  public ConcurrentAdder(int numOfThreads) {
    this.numOfThreads = numOfThreads;
  }

  public long sum(long lowerBound, long upperBound) {
    long range = upperBound - lowerBound;
    long step = (long) Math.ceil(range / ((double) numOfThreads));

    List<AdderRunnable> adders = new ArrayList<>();
    List<Thread> threads = new ArrayList<>();

    long start;
    long end = 0;

    for (int i = 1; i <= this.numOfThreads; i++) {
      start = end + 1;
      if (i != this.numOfThreads) {
        end = step * i;
      } else {
        end = upperBound;
      }

      Adder adder = new Adder(start, end);
      AdderRunnable runnable = new AdderRunnable(adder);
      Thread thread = new Thread(runnable);

      threads.add(thread);
      adders.add(runnable);
    }

    ConcurrencyUtils.startThreads(threads);
    try {
      ConcurrencyUtils.joinThreads(threads);
    } catch (InterruptedException e) {
      System.err.println(e.getMessage());
      throw new RuntimeException(e);
    }

    return AdderRunnable.sumAllResults(adders);
  }


}
