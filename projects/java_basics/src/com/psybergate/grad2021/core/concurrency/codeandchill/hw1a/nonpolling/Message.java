package com.psybergate.grad2021.core.concurrency.codeandchill.hw1a.nonpolling;

public class Message<T> {
  private final T t;

  public Message(T t) {
    this.t = t;
  }

  public T getMessage() {
    return t;
  }
}
