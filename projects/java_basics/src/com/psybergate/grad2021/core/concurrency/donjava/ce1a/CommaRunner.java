package com.psybergate.grad2021.core.concurrency.donjava.ce1a;

public class CommaRunner extends Thread{
  private final Comma comma;

  public CommaRunner(Comma comma) {
    this.comma = comma;
  }

  @Override
  public void run() {
    comma.runItUp(this.getName());
  }
}
