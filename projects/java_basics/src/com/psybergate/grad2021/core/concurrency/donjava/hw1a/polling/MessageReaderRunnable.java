package com.psybergate.grad2021.core.concurrency.donjava.hw1a.polling;

public class MessageReaderRunnable implements Runnable{
  private MessageReader reader;

  public MessageReaderRunnable(MessageReader reader) {
    this.reader = reader;
  }

  @Override
  public void run() {
    while (true) {
      boolean quit = false;
      while (reader.hasNewMessage()) {
        Message message = reader.read();
        System.out.println("New message: " + message.getMessage());

        if (message.getMessage().equals("quit")) {
          quit = true;
          break;
        }
      }

      if (quit) {
        break;
      }

      try {
        Thread.sleep(((int) (Math.random() * 500)));
      } catch (InterruptedException e) {
        System.err.println(e.getMessage());
        e.printStackTrace();
      }
    }
  }
}
