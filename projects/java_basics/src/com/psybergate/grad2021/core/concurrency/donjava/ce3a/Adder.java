package com.psybergate.grad2021.core.concurrency.donjava.ce3a;

public class Adder{
  private long lowerBound;
  private long upperBound;

  public Adder(long lowerBound, long upperBound) {
    this.lowerBound = lowerBound;
    this.upperBound = upperBound;
  }

  public long sum() {
    long result = 0;
    for (long i = this.lowerBound; i <= this.upperBound; i++) {
      result += i;
    }

    return result;
  }
}
