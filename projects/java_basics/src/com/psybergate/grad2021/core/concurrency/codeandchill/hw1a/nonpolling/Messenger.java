package com.psybergate.grad2021.core.concurrency.codeandchill.hw1a.nonpolling;

import com.psybergate.grad2021.core.concurrency.donjava.hw1a.nonpolling.Message;
import com.psybergate.grad2021.core.concurrency.donjava.hw1a.nonpolling.MessageReader;
import com.psybergate.grad2021.core.concurrency.donjava.hw1a.nonpolling.MessageWriter;

public class Messenger {
  private final MessageReader reader;

  private final MessageWriter writer;

  public Messenger() {
    this.reader = new MessageReader();
    this.writer = new MessageWriter();
  }

//  public <T> boolean send(T t) {
//    Message<T> message = new Message<>(t);
//    return this.writer.write(message);
//  }

  public boolean send(String string) {
    Message message = new Message(string);
    return this.writer.write(message);
  }

  public Message read() {
    return this.reader.read();
  }
}
