package com.psybergate.grad2021.core.concurrency.donjava.hw1a.polling;

import java.util.ArrayList;
import java.util.List;

public class MessageCache {
  private static final MessageCache MESSAGE_CACHE;

  static {
    MESSAGE_CACHE = new MessageCache();
  }

  public static MessageCache getInstance() {
    return MESSAGE_CACHE;
  }

  public static void main(String[] args) {
    MessageReader reader = new MessageReader();
    MessageWriter writer = new MessageWriter();

    MessageReaderRunnable readerRunnable =
        new MessageReaderRunnable(reader);
    MessageWriterRunnable writerRunnable =
        new MessageWriterRunnable(writer);

    Thread readerThread = new Thread(readerRunnable);
    Thread writerThread = new Thread(writerRunnable);

    readerThread.start();
    writerThread.start();

    try {
      writerThread.join();
      readerThread.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private final List<Message> messages;

  private MessageCache() {
    this.messages = new ArrayList<>();
  }

  public synchronized boolean add(Message message) {
    return this.messages.add(message);
  }

  public synchronized Message read() {
    if (this.messages.isEmpty()) {
      return null;
    }
    return this.messages.remove(0);
  }

  public synchronized boolean isEmpty() {
    return this.messages.isEmpty();
  }
}
