package com.psybergate.grad2021.core.concurrency.donjava.ce1a;

public class Runner implements Runnable{

  private final int num;

  public Runner(int num) {
    this.num = num;
  }

  @Override
  public void run() {
    System.out.println("Runner " + num + ": now running it up...");
    try {
      Thread.sleep(50);
    } catch (InterruptedException e) {
      System.out.println("Runner " + num + ": couldn't run up the whole thing" +
          "...");
      e.printStackTrace();
    }
    System.out.println("Runner " + num + ": ran it up successfully...");
  }
}
