package com.psybergate.grad2021.core.concurrency.donjava.hw1a.polling;


public class MessageWriterRunnable implements Runnable {
  private MessageWriter writer;

  public MessageWriterRunnable(MessageWriter writer) {
    this.writer = writer;
  }

  @Override
  public void run() {
    for (int i = 0; i < 10; i++) {
      Message message =
          new Message(String.format("%.2f", Math.random() * 100000));
      if (this.writer.write(message)) {
        System.out.println("Message Written: " + message.getMessage());
      } else {
        System.out.println("Something went wrong....");
      }
      try {
        Thread.sleep((long) (Math.random() * 1000));
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
    Message quit = new Message("quit");
    this.writer.write(quit);
  }
}
