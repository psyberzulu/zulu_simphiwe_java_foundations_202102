package com.psybergate.grad2021.core.concurrency.donjava.ce3a;

import java.util.List;

public class ConcurrencyUtils {

  public static void main(String[] args) {
//    Serial implementation
    Adder adder = new Adder(1, 1_000_000_000);
    Timer timer = new Timer();

    timer.start();
    long sum = adder.sum();
    timer.stop();

    long serialTime = timer.getDuration();
    System.out.println("Serial Adder: \n\tResult: " + sum + "\n\tTime: " + serialTime + "ms");

//    Concurrent implementation
    ConcurrentAdder concurrentAdder = new ConcurrentAdder(8);
    timer.start();
    long sum1 = concurrentAdder.sum(1, 1_000_000);
    timer.stop();

    long parallelTime = timer.getDuration();
    System.out
        .println("Parallel Adder:\n\tResult: " + sum + "\n\tTime: " + parallelTime + "ms");
  }

  public static void startThreads(List<Thread> threads) {
    for (Thread thread : threads) {
      thread.start();
    }
  }

  public static void joinThreads(List<Thread> threads) throws InterruptedException {
    for (Thread thread : threads) {
      thread.join();
    }
  }
}
