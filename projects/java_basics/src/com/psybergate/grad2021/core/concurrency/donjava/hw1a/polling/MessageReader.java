package com.psybergate.grad2021.core.concurrency.donjava.hw1a.polling;

public class MessageReader {
  private MessageCache cache;

  public MessageReader() {
    this.cache = MessageCache.getInstance();
  }

  public Message read() {
    return this.cache.read();
  }

  public boolean hasNewMessage() {
    return !this.cache.isEmpty();
  }
}
