package com.psybergate.grad2021.core.collections.donjava.hw4a.singlylinkedlist;

public class DonNode {
  private Object element;
  private DonNode next;

  public DonNode(Object element, DonNode next) {
    this.element = element;
    this.next = next;
  }

  public DonNode(Object element) {
    this(element, null);
  }

  public Object getElement() {
    return element;
  }

  public DonNode getNext() {
    return next;
  }

  public void setElement(Object element) {
    this.element = element;
  }

  public void setNext(DonNode next) {
    this.next = next;
  }
}
