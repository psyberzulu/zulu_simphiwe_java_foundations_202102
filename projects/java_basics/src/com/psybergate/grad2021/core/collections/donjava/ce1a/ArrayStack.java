//package com.psybergate.grad2021.core.collections.donjava.ce1a;
//
//import java.util.Iterator;
//
//public class ArrayStack implements DonStack {
//
//  private final Object[] objects;
//
//  public ArrayStack(int size) {
//    this.objects = new Object[size];
//  }
//
//  @Override
//  public Object get(int index) {
//    return this.objects[index];
//  }
//
//  @Override
//  public Object pop() {
//    if (this.isEmpty()) {
//      return null;
//    }
//    Object o = this.objects[this.size() - 1];
//    this.objects[this.size() - 1] = null;
//    return o;
//  }
//
//  @Override
//  public boolean push(Object o) {
//    if (this.isFull()) {
//      return false;
//    }
//
//    this.objects[this.size() + 1] = o;
//    return true;
//  }
//
//  @Override
//  public int size() {
//    int size = 0;
//    for (Object o : objects) {
//      if (o == null) {
//        break;
//      }
//      size++;
//    }
//    return size;
//  }
//
//  @Override
//  public Iterator<Object> iterator(){
//    return new ArrayStackIterator<Object>();
//  }
//
//  private boolean isFull() {
//    return this.objects.length == this.size();
//  }
//
//  @Override
//  public boolean isEmpty() {
//    return this.size() == 0;
//  }
//
//  @Override
//  public boolean contains(Object o) {
//    for (Object object: this.objects) {
//      if (object.equals(o)) {
//        return true;
//      }
//    }
//    return false;
//  }
//
//  private class ArrayStackIterator aimplements Iterator<Object>{
//
//    private int currentIndex = 0;
//
//    public boolean hasNext() {
//      return size() > currentIndex;
//    }
//
//    public Object next(){
//      return objects[currentIndex];
//    }
//  }
//
//
//
//}
