package com.psybergate.grad2021.core.collections.donjava.hw1a;

public class Administrator extends Employee {
    public static final VariableContribution MEDICAL_AID_CONTRIBUTION =
            new VariableContribution(10, 800, 2000);

    public Administrator(String employeeNum, String name, String surname,
                         double annualSalary) {
        super(employeeNum, name, surname, annualSalary);
    }

    @Override
    public double getMedicalAidContribution() {
        return MEDICAL_AID_CONTRIBUTION.getContributionAmount(this.annualSalary);
    }
}
