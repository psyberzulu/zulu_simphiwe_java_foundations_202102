package com.psybergate.grad2021.core.collections.donjava.hw5a;

import java.util.*;

public class DonBinarySearchTree implements List<Object> {
  private DonTreeNode root;
  private Comparator<Object> comparator;

  private int size;

  public DonBinarySearchTree(DonTreeNode root, Comparator<Object> comparator) {
    this.root = root;
    this.comparator = comparator;
    this.size = 0;
  }

  public DonBinarySearchTree() {
    this(null, null);
  }

  @Override
  public int size() {
    return this.size;
  }

  @Override
  public boolean isEmpty() {
    return this.size() > 0;
  }

  @Override
  public boolean contains(Object o) {
    DonTreeNode current = this.root;
    while (current != null) {
      if (o.equals(current.getElement())) {
        return true;
      }

      if (comparator.compare(o, current.getElement()) > -1) {
        if (current.hasRightChild()) {
          current = current.getRightChild();
        } else {
          return false;
        }
      } else {
        if (current.hasLeftChild()) {
          current = current.getLeftChild();
        } else {
          return false;
        }
      }

    }
    return false;
  }

  @Override
  public Iterator<Object> iterator() {
    return null;
  }

  @Override
  public Object[] toArray() {
    DonTreeNode current = this.root;

    return new Object[0];
  }

  public int getHeight(DonTreeNode node) {
    if (this.isEmpty()) {
      return 0;
    }

    int leftHeight = this.getHeight(node.getLeftChild());
    int rightHeight = this.getHeight(node.getRightChild());

    if (leftHeight > rightHeight) {
      return leftHeight + 1;
    }

    return rightHeight + 1;
  }

  @Override
  public <T> T[] toArray(T[] a) {
    return null;
  }

  @Override
  public boolean add(Object o) {
    DonTreeNode node = new DonTreeNode(o);

    if (this.isEmpty()) {
      this.root = node;
      this.size++;
      return true;
    }

    DonTreeNode current = this.root;

    while (current != null) {
      if (comparator.compare(o, current.getElement()) > -1) {
        if (current.hasRightChild()) {
          current = current.getRightChild();
          continue;
        }

        current.setRight(node);
        node.setParent(current);
        this.size++;
        return true;
      }

      if (current.hasLeftChild()) {
        current = current.getLeftChild();
        continue;
      }

      current.setLeft(node);
      node.setParent(current);
      this.size++;
      return true;
    }
    return false;
  }

  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean addAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean addAll(int index, Collection<?> c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public Object get(int index) {
    return null;
  }

  @Override
  public Object set(int index, Object element) {
    return null;
  }

  @Override
  public void add(int index, Object element) {

  }

  @Override
  public Object remove(int index) {
    return null;
  }

  @Override
  public int indexOf(Object o) {
    return 0;
  }

  @Override
  public int lastIndexOf(Object o) {
    return 0;
  }

  @Override
  public ListIterator<Object> listIterator() {
    return null;
  }

  @Override
  public ListIterator<Object> listIterator(int index) {
    return null;
  }

  @Override
  public List<Object> subList(int fromIndex, int toIndex) {
    return null;
  }
}
