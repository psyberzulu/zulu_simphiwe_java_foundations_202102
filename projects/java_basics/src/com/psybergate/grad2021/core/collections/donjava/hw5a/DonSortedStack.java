package com.psybergate.grad2021.core.collections.donjava.hw5a;

import com.psybergate.grad2021.core.collections.donjava.ce1a.DonStack;

public interface DonSortedStack extends DonStack {
  void sort();
}
