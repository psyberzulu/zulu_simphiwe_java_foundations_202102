package com.psybergate.grad2021.core.collections.donjava.hw4a.doublylinkedlist;

public class DonNode {
  private Object element;
  private DonNode next;
  private DonNode previous;

  public DonNode(Object element, DonNode next, DonNode previous) {
    this.element = element;
    this.next = next;
    this.previous = previous;
  }

  public DonNode(Object element) {
    this(element, null, null);
  }

  public Object getElement() {
    return element;
  }

  public DonNode getNext() {
    return next;
  }

  public DonNode getPrevious() {
    return previous;
  }

  public void setElement(Object element) {
    this.element = element;
  }

  public void setNext(DonNode next) {
    this.next = next;
  }

  public void setPrevious(DonNode previous) {
    this.previous = previous;
  }
}
