package com.psybergate.grad2021.core.collections.donjava.hw1a;

public abstract class Employee implements Comparable<Employee>{
    private String employeeNum;
    private String name;
    private String surname;
    protected double annualSalary;

    public Employee(String employeeNum, String name, String surname,
                    double annualSalary) {
        this.employeeNum = employeeNum;
        this.name = name;
        this.surname = surname;
        this.annualSalary = annualSalary;
    }

    public abstract double getMedicalAidContribution();

    @Override
    public int compareTo(Employee e) {
        if (this.equals(e)) {
            return 0;
        }
        return this.employeeNum.compareTo(e.employeeNum);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null) {
            return false;
        }

        if (this.getClass() != o.getClass()) {
            return false;
        }

        Employee e = ((Employee) o);
        return this.employeeNum.equals(e.employeeNum);
    }

    @Override
    public String toString() {
        return "Employee{" +
            "employeeNum='" + employeeNum + '\'' +
            ", name='" + name + '\'' +
            ", surname='" + surname + '\'' +
            ", annualSalary=" + String.format(" R%.2f", annualSalary) +
            '}';
    }

    public String getEmployeeNum() {
        return employeeNum;
    }

    public double getAnnualSalary() {
        return annualSalary;
    }
}
