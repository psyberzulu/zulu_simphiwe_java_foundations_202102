package com.psybergate.grad2021.core.collections.donjava.hw2a;
import com.psybergate.grad2021.core.collections.donjava.hw1a.Administrator;
import com.psybergate.grad2021.core.collections.donjava.hw1a.Director;
import com.psybergate.grad2021.core.collections.donjava.hw1a.Employee;
import com.psybergate.grad2021.core.collections.donjava.hw1a.Manager;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MedicalAidUtils {
  public static void main(String[] args) {
    Map employeeMap = generateList(10);
    printMap(employeeMap);
  }

  public static Map<Employee, Double> generateList(int size) {
    Map employees = new TreeMap();
    for (int i = 0; i < size; i++) {
      int random = ((int) (Math.random() * 3));
      Employee employee;
      switch (random) {
        case 1:
          employee = new Manager("M" + i, "Manna " + i, "Manager", (Math
              .random() * 5000) + 150000);
          break;
        case 2:
          employee = new Director("D" + i, "Dier " + i, "Director", (Math
              .random() * 5000) + 200000);
          break;
        default:
          employee = new Administrator("A" + i, "Ada " + i, "Admin", (Math
              .random() * 5000) + 100000);
          break;
      }
      employees.put(employee, employee.getAnnualSalary());
    }
    return employees;
  }
  
  public static void printMap(Map<Employee, Double> employeeMap){
    Set<Employee> employees = employeeMap.keySet();
    for (Employee employee : employees) {
      double salary = employeeMap.get(employee);
      System.out.println(employee.getEmployeeNum() + " salary:" + String
          .format(" R%.2f", salary));
    }
  }
}
