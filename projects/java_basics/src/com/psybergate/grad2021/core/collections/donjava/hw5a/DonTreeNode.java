package com.psybergate.grad2021.core.collections.donjava.hw5a;

public class DonTreeNode {
  private Object element;
  private DonTreeNode left;
  private DonTreeNode parent;
  private DonTreeNode right;

  public DonTreeNode(Object element, DonTreeNode parent) {
    this.element = element;
    this.parent = parent;
    this.left = null;
    this.right = null;
  }

  public DonTreeNode(Object element) {
    this(element, null);
  }

  public Object getElement() {
    return element;
  }

  public DonTreeNode getLeftChild() {
    return left;
  }

  public DonTreeNode getParent() {
    return parent;
  }

  public DonTreeNode getRightChild() {
    return right;
  }

  public int getDegree() {
    int degree = 0;
    if (this.left != null) {
      degree++;
    }
    if (this.right != null) {
      degree++;
    }
    return degree;
  }

  public boolean hasLeftChild() {
    return this.left != null;
  }
  public boolean hasRightChild() {
    return this.right != null;
  }

  public boolean hasChildren() {
    return this.hasLeftChild() && this.hasRightChild();
  }

  public Object setElement(Object element) {
    Object previous = this.element;
    this.element = element;
    return previous;
  }

  public void setLeft(DonTreeNode left) {
    this.left = left;
  }

  public void setParent(DonTreeNode parent) {
    this.parent = parent;
  }

  public void setRight(DonTreeNode right) {
    this.right = right;
  }
}
