package com.psybergate.grad2021.core.collections.donjava.ce1a;

import java.util.Collection;

public interface DonStack extends Collection<Object> {

  boolean push(Object o);

  Object pop();

  Object get(int position);
}