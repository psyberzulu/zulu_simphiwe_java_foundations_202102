package com.psybergate.grad2021.core.collections.donjava.hw1a;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MedicalAidUtils {
  public static void main(String[] args) {
    List employees = generateList(10);
    printEmployees(employees);
  }

  public static List<Employee> generateList(int size) {
    List<Employee> list = new ArrayList();
    for (int i = 0; i < size; i++) {
      int random = ((int) (Math.random() * 3));
      Employee employee;
      switch (random) {
        case 1:
          employee = new Manager("M" + i, "Manna " + i, "Manager", (Math
              .random() * 5000) + 150000);
          break;
        case 2:
          employee = new Director("D" + i, "Dier " + i, "Director", (Math
              .random() * 5000) + 200000);
          break;
        default:
          employee = new Administrator("A" + i, "Ada " + i, "Admin", (Math
              .random() * 5000) + 100000);
          break;
      }
      list.add(employee);
    }
    return list;
  }

  public static void printEmployees(List<Employee> employees) {
    Collections.sort(employees);
    for (Employee employee: employees) {
      System.out.println(employee.toString());
    }
  }
}
