package com.psybergate.grad2021.core.collections.donjava.hw4a.singlylinkedlist;

import java.util.Iterator;

public class DonNodeIterator implements Iterator<DonNode> {
  private DonNode currentNode;

  public DonNodeIterator(DonLinkedList list) {
    this.currentNode = list.getHead();
  }

  @Override
  public boolean hasNext() {
    return this. currentNode.getNext() != null;
  }

  @Override
  public DonNode next() {
    currentNode = currentNode.getNext();
    return currentNode;
  }
}
