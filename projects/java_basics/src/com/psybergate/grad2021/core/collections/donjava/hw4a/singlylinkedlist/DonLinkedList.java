package com.psybergate.grad2021.core.collections.donjava.hw4a.singlylinkedlist;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class DonLinkedList implements List<Object> {
  private DonNode head;

  public DonLinkedList(DonNode head) {
    this.head = head;
  }

  public DonLinkedList() {
    this(null);
  }

  @Override
  public boolean add(Object o) {
    this.add(this.size(), o);
    return this.contains(o);
  }

  public void add(int index, Object element) {
    DonNode newNode = new DonNode(element);
    if (this.isEmpty()) {
      this.head = newNode;
    } else if (index == 0) {
      newNode.setNext(this.head);
      this.head = newNode;
    } else if (index < this.size()) {
      newNode.setNext(this.getNode(index));
      this.getNode(index - 1).setNext(newNode);
    } else if (index == this.size()){
      this.getNode(index - 1).setNext(newNode);
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  @Override
  public boolean addAll(int index, Collection<?> c) {
    int i = 0;
    for (Object o : c) {
      this.add(index + i, o);
    }
    return this.containsAll(c);
  }

  @Override
  public boolean addAll(Collection<?> c) {
    boolean added = true;
    for (Object o : c) {
      added = added && this.add(o);
    }
    return added;
  }

  @Override
  public boolean isEmpty() {
    return this.size() == 0;
  }

  @Override
  public boolean contains(Object o) {
    return this.indexOf(o) > -1;
  }

  @Override
  public Iterator<Object> iterator() {
    return new DonListIterator(this);
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public <T> T[] toArray(T[] a) {
    return null;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    boolean contains = true;
    for (Object o :
        c) {
      contains = contains && this.contains(o);
    }
    return contains;
  }

  @Override
  public Object remove(int index) {
    if (index == 0) {
      DonNode current = this.head;
      this.head = head.getNext();
      return current.getElement();
    }

    DonNode previous = this.getNode(index - 1);
    DonNode current = previous.getNext();

    previous.setNext(current.getNext());
    return current.getElement();
  }

  @Override
  public boolean remove(Object o) {
    int index = this.indexOf(o);
    if (index < 0) {
      return false;
    }

    return this.remove(index) != null;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    boolean removed = true;
    for (Object o : c) {
      removed = removed && this.remove(o);
    }
    return removed;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public Object set(int index, Object element) {
    DonNode node = this.getNode(index);
    if (node == null) {
      return null;
    }
    Object previousElement = node.getElement();
    node.setElement(element);
    return previousElement;
  }

  @Override
  public int indexOf(Object o) {
    for (int i = 0; i < this.size(); i++) {
      Object element = this.get(i);
      if (o.equals(element)) {
        return i;
      }
    }
    return -1;
  }

  @Override
  public int lastIndexOf(Object o) {
    int index = -1;
    int i = 0;
    for (Iterator iterator = this.iterator(); iterator.hasNext() ;) {
      Object element = iterator().next();
      if (o.equals(iterator().next())) {
        index = i;
      }
    }
    return index;
  }

  @Override
  public ListIterator<Object> listIterator() {
    return new DonListIterator(this);
  }

  @Override
  public ListIterator<Object> listIterator(int index) {
    return null;
  }

  @Override
  public List<Object> subList(int fromIndex, int toIndex) {
    return null;
  }

  @Override
  public int size() {
    int size = 0;
    for (DonNode current = this.head; current != null; current =
        current.getNext()) {
      size++;
    }
    return size;
  }

  @Override
  public Object get(int index) {
    return this.getNode(index).getElement();
  }

  public DonNode getNode(int index) {
    if (index < 0 || index >= this.size()) {
      throw new IndexOutOfBoundsException();
    }

    DonNode node = this.head;
    for (int i = 0; i < index; i++) {
      node = node.getNext();
    }
    return node;
  }

  public DonNode getHead() {
    return head;
  }

  private void setHead(DonNode head) {
    this.head = head;
  }
}
