package com.psybergate.grad2021.core.collections.donjava.hw3a;

import java.util.HashSet;
import java.util.Set;

public class StringUtils {
  public static void main(String[] args) {
    Set set = generateStringSet(10);
//    printSet(set);
    Set reverseSet = processSet(set);
    printSet(reverseSet);
  }

  public static Set<String> generateStringSet(int size) {
    Set<String> stringSet = new HashSet<String>();
    for (int i = 0; i < size; i++) {
      stringSet.add(String.format("%.6f", Math.random() * size));
    }
    return stringSet;
  }

  public static Set<String> processSet(Set<String> stringSet) {
    Set<String> reverseSet = new HashSet<String>();
    for (String string : stringSet) {
      reverseSet.add(reverseString(string));
    }
    return reverseSet;
  }

  public static String reverseString(String string) {
    String[] characters = string.split("");
    String reverse = "";

    for (int i = characters.length - 1; i > -1; i--) {
      reverse += characters[i];
    }

    return reverse;
  }

  public static void printSet(Set<String> stringSet) {
    for (String string : stringSet) {
      System.out.println(string);
    }
  }
}
