package com.psybergate.grad2021.core.collections.donjava.hw4a.doublylinkedlist;

import java.util.ListIterator;
import java.util.NoSuchElementException;

public class DonListIterator implements ListIterator<Object> {
  private DonNode currentNode;

  private final DonDoublyLinkedList list;

  public DonListIterator(DonNode currentNode, DonDoublyLinkedList list) {
    this.currentNode = currentNode;
    this.list = list;
  }

  public DonListIterator(DonDoublyLinkedList list) {
    this(list.getHead(), list);
  }

  @Override
  public Object next() {
    validateList();
    DonNode node = currentNode;
    this.currentNode = currentNode.getNext();
    return node.getElement();
  }

  @Override
  public int nextIndex() {
    return this.list.indexOf(currentNode.getElement()) + 1;
  }

  @Override
  public boolean hasPrevious() {
    int index = this.list.indexOf(currentNode.getElement());
    return index < 1;
  }

  @Override
  public Object previous() {
    validateList();
    if (this.hasPrevious()) {
      int index = this.list.indexOf(currentNode.getElement());
      Object element = this.currentNode.getElement();
      this.currentNode = this.list.getNode(index - 1);
      return element;
    }
    throw new NoSuchElementException();
  }

  @Override
  public int previousIndex() {
    return this.list.indexOf(currentNode.getElement()) - 1;
  }

  @Override
  public void remove() {

  }

  @Override
  public void set(Object o) {

  }

  @Override
  public void add(Object o) {

  }

  @Override
  public boolean hasNext() {
    return !this.list.isEmpty() && this.currentNode.getNext() != null;
  }

  private void validateList() {
    if (this.list.isEmpty()) {
      throw new NoSuchElementException();
    }
  }
}
