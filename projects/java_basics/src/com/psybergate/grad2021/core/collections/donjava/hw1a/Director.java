package com.psybergate.grad2021.core.collections.donjava.hw1a;

public class Director extends Employee {
    public static final Contribution MEDICAL_AID_CONTRIBUTION =
            new Contribution(5000);

    public Director(String employeeNum, String name, String surname,
                    double annualSalary) {
        super(employeeNum, name, surname, annualSalary);
    }

    @Override
    public double getMedicalAidContribution() {
        return MEDICAL_AID_CONTRIBUTION.getContribution();
    }
}
