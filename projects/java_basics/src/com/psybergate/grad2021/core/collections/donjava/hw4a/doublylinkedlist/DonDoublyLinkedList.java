package com.psybergate.grad2021.core.collections.donjava.hw4a.doublylinkedlist;

import java.util.*;

public class DonDoublyLinkedList implements List<Object> {
  private DonNode head;

  private DonNode tail;

  public DonDoublyLinkedList() {
    this.head = null;
    this.tail = null;
  }

  @Override
  public int size() {
    int size = 0;
    for (DonNode current = this.head; current != null;
         current = current.getNext()) {
      size++;
    }
    return size;
  }

  @Override
  public boolean isEmpty() {
    return this.size() == 0;
  }

  @Override
  public boolean contains(Object o) {
    for (DonNode current = this.head; current != null;
         current = current.getNext()) {
      if (o.equals(current.getElement())) {
        return true;
      }
    }
    return false;
  }

  @Override
  public Iterator<Object> iterator() {
    return new DonListIterator(this);
  }

  @Override
  public Object[] toArray() {
    return new Object[0];
  }

  @Override
  public <T> T[] toArray(T[] a) {
    return null;
  }

  @Override
  public boolean add(Object o) {
    this.add(this.size(), o);
    return this.contains(o);
  }

  @Override
  public boolean remove(Object o) {
    return false;
  }

  @Override
  public boolean containsAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean addAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean addAll(int index, Collection<?> c) {
    return false;
  }

  @Override
  public boolean removeAll(Collection<?> c) {
    return false;
  }

  @Override
  public boolean retainAll(Collection<?> c) {
    return false;
  }

  @Override
  public void clear() {

  }

  @Override
  public Object get(int index) {
    return this.getNode(index).getElement();
  }

  public DonNode getHead() {
    return this.head;
  }

  @Override
  public Object set(int index, Object element) {
    DonNode node = this.getNode(index);
    if (node == null) {
      return null;
    }
    Object previousElement = node.getElement();
    node.setElement(element);
    return previousElement;
  }

  @Override
  public void add(int index, Object element) {
    DonNode node = new DonNode(element);
    if (this.isEmpty()) {
      this.head = node;
      this.tail = node;
    } else if (index == 0) {
      this.head.setPrevious(node);
      node.setNext(this.head);
      this.head = node;
    } else if (index > 0 && index < this.size()) {
      DonNode current = this.getNode(index);
      DonNode previous = current.getPrevious();
      node.setPrevious(previous);
      node.setNext(current);
      previous.setNext(node);
      current.setPrevious(node);
    } else if (index == this.size()) {
      node.setPrevious(this.tail);
      this.tail.setNext(node);
      this.tail = node;
    } else {
      throw new IndexOutOfBoundsException();
    }
  }

  @Override
  public Object remove(int index) {
    int lastIndex = this.size() - 1;
    DonNode node = this.getNode(index);
    if (index == 0) {
      this.head = node.getNext();
      this.head.setPrevious(null);
    } else if (index > 0 && index < lastIndex) {
      DonNode previous = node.getPrevious();
      DonNode next = node.getNext();
      previous.setNext(next);
      next.setPrevious(previous);
    } else if (index == lastIndex) {
      this.tail = node.getPrevious();
      this.tail.setNext(null);
    } else {
      throw new IndexOutOfBoundsException();
    }
    return node.getElement();
  }

  @Override
  public int indexOf(Object o) {
    int index = 0;
    for (DonNode current = this.head; current != null;
         current = current.getNext()) {
      Object element = current.getElement();
      if (element.equals(o)) {
        return index;
      }
      index++;
    }
    return -1;
  }

  @Override
  public int lastIndexOf(Object o) {
    int index = this.size() - 1;
    for (ListIterator iterator = this.listIterator(index - 1);
         iterator.hasPrevious(); index--) {
      if (o.equals(iterator.previous())) {
        return index;
      }
    }
    return -1;
  }

  @Override
  public ListIterator<Object> listIterator() {
    return new DonListIterator(this);
  }

  @Override
  public ListIterator<Object> listIterator(int index) {
    return null;
  }

  @Override
  public List<Object> subList(int fromIndex, int toIndex) {
    return null;
  }

  public DonNode getNode(int index) {
    if (index < 0 || index >= this.size()) {
      throw new IndexOutOfBoundsException();
    }

    int mid = Math.floorDiv(this.size(), 2);
    DonNode current;
    if (index <= mid) {
      current = this.head;
      for (int i = 0; i < index; i++) {
        current = current.getNext();
      }
    } else {
      current = this.tail;
      for (int i = this.size() - 1; i > index; i--) {
        current = current.getPrevious();
      }
    }
    return current;
  }
}
