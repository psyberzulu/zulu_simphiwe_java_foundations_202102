package com.psybergate.grad2021.core.strings.donjava.ce2a;

public class StringImmutability {
  public static void main(String[] args) {
    String s1 = "I'm a String";

    // Should return false if toLowerCase() only modifies s1's state which
    // would mean that Strings aren't immutable.
    System.out.println("s1 == s1.toLowerCase() = " + (s1 == s1.toLowerCase()));

    // Should return false if replace() only modifies s1 's state which
    // would mean that Strings aren 't immutable
    System.out.println("(s1 == s1.replace(\"i\", \"1\")) = " + (s1 == s1
        .replace("i", "1")));
  }
}
