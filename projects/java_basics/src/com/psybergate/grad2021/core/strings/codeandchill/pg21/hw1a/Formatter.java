package com.psybergate.grad2021.core.strings.codeandchill.pg21.hw1a;

public class Formatter {

    public static void main(String[] args) {
        System.out.println("formatPrice(20.056) = " + formatPrice(20.056));
    }

    public static String formatPrice(double amount) {
        return String.format("R%.2f", amount);
    }
}
