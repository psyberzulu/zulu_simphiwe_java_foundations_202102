package com.psybergate.grad2021.core.strings.donjava.ce1a;

public class StringNotes {
//    String class declaration modifiers:
//      - final

    public static void main(String[] args) {
//      String methods:
        String s = "I'm a String";

//      charAt(i) returns a char found at index i
        System.out.println("s.charAt(0) = " + s.charAt(0));

//      toUpperCase() returns a new String with all its characters in upper case
        System.out.println("s.toUpperCase() = " + s.toUpperCase());

//      compareTo() compares two Strings to each other
        System.out.println("s.compareTo(\"I'm a string\") = " + s.compareTo("I'm a string"));
    }
}
