package com.psybergate.grad2021.core.strings.codeandchill.pg21.ce2a;

public class StringImmutability {

    public static void main(String[] args) {
        String s1 = "I'm a String";

        Object obj = new Object();

//        System.out.println(new Customer() == new Customer());
        String ogFoo = "foo";       // 123

//        System.out.println(new String("Foo")==new String("Foo"));  //False
//        System.out.println("foo"==ogFoo);  // 123 == 123

        String foo1 = "foo"; // ? 123

//        System.out.println(ogFoo == foo1); // 123==123

//        System.out.println(foo1 == new String("foo")); // 123 != ???

        String foobar = foo1.concat("Bar");
//        System.out.println(foobar);   //

        System.out.println("fooBar"== foobar);  // True (Actaully false)
        System.out.println("fooBar"== foobar.intern());  // Actually true??





//        2.toString(); won't work because 2 is a literal
        "Foo".toString();
        // Should return false if toUpperCase() only modifies s1's state which
        // would mean that Strings aren't immutable.
        System.out.println("s1 == s1.toLowerCase() = " + (s1 == s1.toLowerCase()));

//        Should return false if replace() only modifies s1's state which
//        would mean that Strings aren't immutable
        System.out.println("(s1 == s1.replace(\"i\", \"1\")) = " + (s1 == s1.replace("i", "1")));
    }
}
