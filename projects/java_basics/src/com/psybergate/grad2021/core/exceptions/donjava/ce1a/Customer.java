package com.psybergate.grad2021.core.exceptions.donjava.ce1a;

import java.time.LocalDate;

public class Customer {
  private String customerNum;
  private String name;
  private String surname;
  private LocalDate dateOfBirth;

  public Customer(String customerNum, String name, String surname, String dateOfBirth) {
    try {
      LocalDate date = convertStringToDate(dateOfBirth);
      validateCustomer(date);
      this.customerNum = customerNum;
      this.name = name;
      this.surname = surname;
      this.dateOfBirth = date;
    } catch (InvalidCustomerException e) {
//      Printing the stack trace is not handling the exp
      e.printStackTrace();
    }
  }

  private void validateCustomer(LocalDate date) throws InvalidCustomerException {
    if ((date.getYear() - LocalDate.now().getYear()) <= 18){
      throw new InvalidCustomerException("Customer is too young for this level.");
    }
  }

  private static LocalDate convertStringToDate(String dateOfBirth) {
    String[] list = dateOfBirth.split("/");

    int day = Integer.parseInt(list[0]);
    int month = Integer.parseInt(list[1]);
    int year = Integer.parseInt(list[2]);
    return LocalDate.of(year, month, day);
  }

  public static void main(String[] args) {
    Customer myCust = new Customer("110", "Karl", "Kastis", "1/1/2013");
    System.out.println("Still runs even though exception was encountered(??) -> squash successful");
  }
}
