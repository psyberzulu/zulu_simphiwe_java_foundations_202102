package com.psybergate.grad2021.core.exceptions.codeandchill.pg21.ce2a;

import com.psybergate.grad2021.core.exceptions.codeandchill.pg21.ce1a.InvalidCustomerException;

import java.time.LocalDate;
import java.util.Scanner;

public class Customer {
  private String customerNum;
  private String name;
  private String surname;
  private LocalDate dateOfBirth;

  public Customer(String customerNum, String name, String surname, String dateOfBirth) {
    LocalDate date = convertStringToDate(dateOfBirth);
//    validateCustomer(customerNum, name, surname, date);
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = date;
  }

  private static LocalDate convertStringToDate(String date) {
    if (date.isEmpty()) {
      throw new IllegalArgumentException("Date should not be empty");
    }

    String[] list = date.split("/");
    int day = Integer.parseInt(list[0]);
    int month = Integer.parseInt(list[1]);
    int year = Integer.parseInt(list[2]);
    return LocalDate.of(year, month, day);
  }

  public static void main(String[] args) {
    try {
      Scanner in = new Scanner(System.in);
      System.out.println("Please enter customer:");
      String customerNum = in.nextLine();
//      "Please enter customer's"
      Customer myCust = new Customer(customerNum, "Karl", "Kastis", "1/1/2013");
    } catch (Throwable t) {

    }
  }

  private void validateCustomer(String customerNum, String name, String surname, LocalDate date) throws InvalidCustomerException {
    try {
      validateCustomerNum(customerNum);
      validateName(name);
      validateCustomerAge(date);
    } catch (Exception e) {
      throw e;
    }
  }

    private void validateCustomerNum(String customerNum) {
      if (customerNum.charAt(0) != 'C') {
          throw new IllegalArgumentException("Customer number is invalid");
      }
    }

    private void validateCustomerAge(LocalDate date) throws InvalidCustomerException {
    if ((date.getYear() - LocalDate.now().getYear()) <= 18) {
      throw new InvalidCustomerException("Customer is too young for this level.");
    }
  }

  private void validateName(String name) throws IllegalArgumentException {
    if (name.isEmpty()) {
      throw new IllegalArgumentException("Customer name cannot be empty.");
    }
  }


}
