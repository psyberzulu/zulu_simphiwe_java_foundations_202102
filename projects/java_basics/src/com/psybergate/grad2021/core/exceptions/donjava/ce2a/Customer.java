package com.psybergate.grad2021.core.exceptions.donjava.ce2a;

import com.psybergate.grad2021.core.exceptions.donjava.ce1a.InvalidCustomerException;

import java.time.LocalDate;

public class Customer {
  private String customerNum;
  private String name;
  private String surname;
  private LocalDate dateOfBirth;

  public Customer(String customerNum, String name, String surname, String dateOfBirth) {
    LocalDate date = convertStringToDate(dateOfBirth);
    validateCustomer(customerNum, name, surname, date);
    this.customerNum = customerNum;
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = date;
  }

  private static LocalDate convertStringToDate(String date) {
    if (date.isEmpty()) {
      throw new IllegalArgumentException("Date should not be empty");
    }

    String[] list = date.split("/");
    int day = Integer.parseInt(list[0]);
    int month = Integer.parseInt(list[1]);
    int year = Integer.parseInt(list[2]);
    return LocalDate.of(year, month, day);
  }

  public static void main(String[] args) {
    Customer myCust = new Customer("110", "Karl", "Kastis", "1/1/2013");
    System.out.println("Still runs even though exception was encountered(??) -> squash successful");
  }

  private void validateCustomer(String customerNum, String name, String surname, LocalDate date) {
    try {
//      validateCustomerNum(customerNum);
      validateName(name);
//      validateSurname(surname);
      validateCustomerAge(date);
    } catch (Exception e) {

    }
  }

  private void validateCustomerAge(LocalDate date) throws InvalidCustomerException {
    if ((date.getYear() - LocalDate.now().getYear()) <= 18) {
      throw new InvalidCustomerException("Customer is too young for this level.");
    }
  }

  private void validateName(String name) throws IllegalArgumentException {
    if (name.isEmpty()) {
      throw new IllegalArgumentException("Customer name cannot be empty.");
    }
  }


}
