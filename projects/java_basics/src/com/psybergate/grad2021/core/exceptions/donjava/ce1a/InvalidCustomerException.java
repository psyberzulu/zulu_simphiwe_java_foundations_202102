package com.psybergate.grad2021.core.exceptions.donjava.ce1a;

public class InvalidCustomerException extends Exception {
  public InvalidCustomerException() {
    super("Cutomer too young");
  }

  public InvalidCustomerException(String message) {
    super(message);
  }
}
