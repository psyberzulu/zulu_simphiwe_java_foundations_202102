package com.psybergate.grad2021.core.generics.donjava.hw3a;

public class Account {
  private final String accountNum;
  private double balance;

  public Account(String accountNum) {
    this.accountNum = accountNum;
    this.balance = 0;
  }

  public String getAccountNum() {
    return accountNum;
  }

  public double getBalance() {
    return balance;
  }

  public void setBalance(double balance) {
    this.balance = balance;
  }
}
