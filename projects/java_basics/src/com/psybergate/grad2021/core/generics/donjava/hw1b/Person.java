package com.psybergate.grad2021.core.generics.donjava.hw1b;

public class Person {
  protected String personID;
  protected String email;
  protected String name;
  protected String phone;

  public Person(String personID, String email, String name, String phone) {
    this.personID = personID;
    this.email = email;
    this.name = name;
    this.phone = phone;
  }

  public String getPersonID() {
    return personID;
  }

  public void setPersonID(String personID) {
    this.personID = personID;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }
}
