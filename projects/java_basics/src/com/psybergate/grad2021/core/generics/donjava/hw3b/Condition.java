package com.psybergate.grad2021.core.generics.donjava.hw3b;

public interface Condition<T> {
  boolean isSatisfiedBy(T t);
}
