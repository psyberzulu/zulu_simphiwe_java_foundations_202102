package com.psybergate.grad2021.core.generics.donjava.hw3b;

public class OddNumberCondition implements Condition<Integer>{
  @Override
  public boolean isSatisfiedBy(Integer integer) {
    return integer % 2 != 0;
  }
}
