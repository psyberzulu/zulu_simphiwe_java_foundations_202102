package com.psybergate.grad2021.core.generics.donjava.hw1b;

public class Director extends Person{

  private double shareSplit;

  public Director(String personID, String email, String name, String phone, double shareSplit) {
    super(personID, email, name, phone);
    this.shareSplit = shareSplit;
  }
}
