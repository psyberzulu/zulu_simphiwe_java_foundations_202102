package com.psybergate.grad2021.core.generics.donjava.hw3a;

import java.util.Arrays;
import java.util.Collection;

public class CollectionUtils {

  public static int countEvenNumbers(Collection<Integer> integers) {
    int count = 0;
    for (Integer integer : integers) {
      if (isEven(integer)) {
        count++;
      }
    }

    return count;
  }

  public static int countOddNumbers(Collection<Integer> integers) {
    int count = 0;
    for (Integer integer : integers) {
      if (isOdd(integer)) {
        count++;
      }
    }

    return count;
  }

  public static int countPrimeNumbers(Collection<Integer> integers) {
    int count = 0;
    for (Integer integer : integers) {
      if (isPrime(integer)) {
        count++;
      }
    }

    return count;
  }

  public static int countPositiveBalances(Collection<Account> accounts) {
    int count = 0;
    for (Account account : accounts) {
      if (isNegative(account.getBalance())) {
        count++;
      }
    }

    return count;
  }

  public static int countStringsThatStartWithChar(Collection<String> strings,
                                                  char c) {
    int count = 0;
    for (String string : strings) {
      if (stringStartsWith(string, c)) {
        count++;
      }
    }

    return count;
  }

  private static boolean isOdd(Integer integer) {
    return !isEven(integer);
  }

  private static boolean isEven(Integer integer) {
    return (integer % 2) == 0;
  }

  private static boolean isPrime(Integer integer) {
    if (integer < 2) {
      return false;
    }

    int[] flags = new int[integer];

    Arrays.fill(flags, 1);

    for (int i = 2; i < integer + 1; i++) {
      for (int j = 2 * i; j < integer + 1; j+= i) {
        if (flags[j - 1] == 1) {
          flags[j - 1] = 0;
        }
      }
    }

    return flags[integer - 1] == 1;
  }

  public static boolean isNegative(double number) {
    return number < 0;
  }

  private static boolean stringStartsWith(String string, char c) {
    return string.charAt(0) == c;
  }
}
