package com.psybergate.grad2021.core.generics.donjava.hw1c;

import java.util.ArrayList;
import java.util.List;

public class StringUtils {
  public static void main(String[] args) {
    List<String> strings = generateStringList(10);
    List<String> reversed = reverseStrings(strings);

    printStrings(reversed);
  }

  private static void printStrings(List<String> reversed) {
    for (String s : reversed) {
      System.out.println(s);
    }
  }

  public static List<String> generateStringList(int size) {
    List<String> strings = new ArrayList<>();
    for (int i = 0; i < size; i++) {
      strings.add(String.format("%.6f", (Math.random() * size)));
    }

    return strings;
  }


  public static List<String> reverseStrings(List<String> strings) {
    List<String> reversedStrings = new ArrayList<>();

    for (String string : strings) {
      String reversedString = reverseString(string);
      reversedStrings.add(reversedString);
    }

    return reversedStrings;
  }

//  public static List reverseErased(List strings) {
//    List reversedStrings = new ArrayList();
//
//    for (String string : strings) {
//      String reversedString = reverseString(string);
//      reversedStrings.add(reversedString);
//    }
//
//    return reversedStrings;
//  }

  public static String reverseString(String string) {
    String reversed = "";
    String[] split = string.split("");

    for (String s : split) {
      reversed = s + reversed;
    }

    return reversed;
  }
}
