package com.psybergate.grad2021.core.generics.donjava.hw2a;

import java.util.ArrayList;
import java.util.Collection;

public class Util {

  public static Collection<Integer> greaterThan1000(Collection<Integer> integers) {
    Collection<Integer> processedIntegers = new ArrayList<Integer>();

    for (Integer integer : integers) {
      if (integer.compareTo(1000) > 0) {
        processedIntegers.add(integer);
      }
    }

    return processedIntegers;
  }
}
