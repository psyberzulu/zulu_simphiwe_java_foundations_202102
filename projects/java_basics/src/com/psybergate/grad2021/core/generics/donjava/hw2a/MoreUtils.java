package com.psybergate.grad2021.core.generics.donjava.hw2a;

import java.util.ArrayList;
import java.util.Collection;

public class MoreUtils {
  public static void main(String[] args) {
    Collection<Integer> integers = generateIntegerCollection(10);
    Collection<String> strings = generateStringCollection(10);

//    Utils.greaterThan1000(strings);
  }

  public static Collection<Integer> generateIntegerCollection(int size) {
    Collection<Integer> collection = new ArrayList<>();

    for (int i = 0; i < size; i++) {
      collection.add((int) (Math.random() * 1000) * i);
    }

    return collection;
  }

  public static Collection<String> generateStringCollection(int size) {
    Collection<String> collection = new ArrayList<>();

    for (int i = 0; i < size; i++) {
      collection.add(String.valueOf((int) (Math.random() * 1000) * i));
    }

    return collection;
  }
}
