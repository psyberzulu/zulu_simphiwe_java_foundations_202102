package com.psybergate.grad2021.core.generics.donjava.hw3b;

import java.util.Arrays;

public class PrimeNumberCondition implements Condition<Integer>{
  @Override
  public boolean isSatisfiedBy(Integer integer) {
    if (integer < 2) {
      return false;
    }

    int[] flags = new int[integer];

    Arrays.fill(flags, 1);

    for (int i = 2; i < integer + 1; i++) {
      for (int j = 2 * i; j < integer + 1; j += i) {
        if (flags[j - 1] == 1) {
          flags[j - 1] = 0;
        }
      }
    }

    return flags[integer - 1] == 1;
  }
}
