package com.psybergate.grad2021.core.generics.donjava.hw1b;

import java.util.List;

public class Team<E extends Person> {
  private List<E> teamMembers;
  private String name;
  private String teamID;
  private static final int MAXIMUM_NUMBER_OF_MEMBERS = 18;

  public Team(String name, String teamID) {
    this.name = name;
    this.teamID = teamID;
  }

  public void add(E e) {
    this.teamMembers.add(e);
  }

  public void drop(E e) {
    this.teamMembers.remove(e);
  }
}
